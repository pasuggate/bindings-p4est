{-# OPTIONS_GHC -fno-warn-unused-imports #-}
#include <bindings.dsl.h>
#include "p8est_ghost.h"
module Bindings.P8est.P8estGhost where
import Foreign.Ptr
#strict_import


import Bindings.SC.ScContainers
import Bindings.P8est.P8est
import Bindings.P8est.P8estConnectivity


{- typedef struct {
            int mpisize;
            p8est_topidx_t num_trees;
            p8est_connect_type_t btype;
            sc_array_t ghosts;
            p8est_locidx_t * tree_offsets;
            p8est_locidx_t * proc_offsets;
            sc_array_t mirrors;
            p8est_locidx_t * mirror_tree_offsets;
            p8est_locidx_t * mirror_proc_mirrors;
            p8est_locidx_t * mirror_proc_offsets;
            p8est_locidx_t * mirror_proc_fronts;
            p8est_locidx_t * mirror_proc_front_offsets;
        } p8est_ghost_t; -}
#starttype p8est_ghost_t
#field mpisize , CInt
#field num_trees , CInt
#field btype , <p8est_connect_type_t>
#field ghosts , <struct sc_array>
#field tree_offsets , Ptr CInt
#field proc_offsets , Ptr CInt
#field mirrors , <struct sc_array>
#field mirror_tree_offsets , Ptr CInt
#field mirror_proc_mirrors , Ptr CInt
#field mirror_proc_offsets , Ptr CInt
#field mirror_proc_fronts , Ptr CInt
#field mirror_proc_front_offsets , Ptr CInt
#stoptype

#ccall p8est_ghost_is_valid , Ptr <struct p8est> -> Ptr <p8est_ghost_t> -> IO CInt
#ccall p8est_ghost_memory_used , Ptr <p8est_ghost_t> -> IO CSize
#ccall p8est_quadrant_find_owner , Ptr <struct p8est> -> CInt -> CInt -> Ptr <struct p8est_quadrant> -> IO CInt
#ccall p8est_ghost_new , Ptr <struct p8est> -> <p8est_connect_type_t> -> IO (Ptr <p8est_ghost_t>)
#ccall p8est_ghost_destroy , Ptr <p8est_ghost_t> -> IO ()
#ccall p8est_ghost_bsearch , Ptr <p8est_ghost_t> -> CInt -> CInt -> Ptr <struct p8est_quadrant> -> IO CLong
#ccall p8est_ghost_contains , Ptr <p8est_ghost_t> -> CInt -> CInt -> Ptr <struct p8est_quadrant> -> IO CLong
#ccall p8est_face_quadrant_exists , Ptr <struct p8est> -> Ptr <p8est_ghost_t> -> CInt -> Ptr <struct p8est_quadrant> -> Ptr CInt -> Ptr CInt -> Ptr CInt -> IO CInt
#ccall p8est_quadrant_exists , Ptr <struct p8est> -> Ptr <p8est_ghost_t> -> CInt -> Ptr <struct p8est_quadrant> -> Ptr <struct sc_array> -> Ptr <struct sc_array> -> Ptr <struct sc_array> -> IO CInt
#ccall p8est_is_balanced , Ptr <struct p8est> -> <p8est_connect_type_t> -> IO CInt
#ccall p8est_ghost_checksum , Ptr <struct p8est> -> Ptr <p8est_ghost_t> -> IO CUInt
#ccall p8est_ghost_exchange_data , Ptr <struct p8est> -> Ptr <p8est_ghost_t> -> Ptr () -> IO ()
#ccall p8est_ghost_exchange_custom , Ptr <struct p8est> -> Ptr <p8est_ghost_t> -> CSize -> Ptr (Ptr ()) -> Ptr () -> IO ()
#ccall p8est_ghost_exchange_custom_levels , Ptr <struct p8est> -> Ptr <p8est_ghost_t> -> CInt -> CInt -> CSize -> Ptr (Ptr ()) -> Ptr () -> IO ()
#ccall p8est_ghost_expand , Ptr <struct p8est> -> Ptr <p8est_ghost_t> -> IO ()
