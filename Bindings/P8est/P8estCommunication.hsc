{-# OPTIONS_GHC -fno-warn-unused-imports #-}
#include <bindings.dsl.h>
#include "p8est_communication.h"
module Bindings.P8est.P8estCommunication where
import Foreign.Ptr
#strict_import

import Bindings.SC.ScMpi
import Bindings.P8est.P8est


#if 1 /* new_P8 */
------------------------------------------------------------------------------
#ccall p8est_comm_parallel_env_assign , Ptr <struct p8est> -> <MPI_Comm> -> IO ()
#ccall p8est_comm_parallel_env_duplicate , Ptr <struct p8est> -> IO ()
#ccall p8est_comm_parallel_env_release , Ptr <struct p8est> -> IO ()
#ccall p8est_comm_parallel_env_replace , Ptr <struct p8est> -> <MPI_Comm> -> IO ()
#ccall p8est_comm_parallel_env_get_info , Ptr <struct p8est> -> IO ()
#ccall p8est_comm_parallel_env_is_null , Ptr <struct p8est> -> IO CInt
#ccall p8est_comm_parallel_env_reduce , Ptr (Ptr <struct p8est>) -> IO CInt
-- #ccall p8est_comm_parallel_env_reduce_ext , Ptr (Ptr <struct p8est>) -> <MPI_Group> -> CInt -> Ptr (Ptr CInt) -> IO CInt
#ccall p8est_comm_count_quadrants , Ptr <struct p8est> -> IO ()
#ccall p8est_comm_global_partition , Ptr <struct p8est> -> Ptr <struct p8est_quadrant> -> IO ()
#ccall p8est_comm_count_pertree , Ptr <struct p8est> -> Ptr CLong -> IO ()
#ccall p8est_comm_is_empty , Ptr <struct p8est> -> CInt -> IO CInt
#ccall p8est_comm_is_contained , Ptr <struct p8est> -> CInt -> Ptr <struct p8est_quadrant> -> CInt -> IO CInt
#ccall p8est_comm_is_owner , Ptr <struct p8est> -> CInt -> Ptr <struct p8est_quadrant> -> CInt -> IO CInt
#ccall p8est_comm_find_owner , Ptr <struct p8est> -> CInt -> Ptr <struct p8est_quadrant> -> CInt -> IO CInt
#ccall p8est_comm_tree_info , Ptr <struct p8est> -> CInt -> Ptr CInt -> Ptr CInt -> Ptr (Ptr <struct p8est_quadrant>) -> Ptr (Ptr <struct p8est_quadrant>) -> IO ()
#ccall p8est_comm_neighborhood_owned , Ptr <struct p8est> -> CInt -> Ptr CInt -> Ptr CInt -> Ptr <struct p8est_quadrant> -> IO CInt
#ccall p8est_comm_sync_flag , Ptr <struct p8est> -> CInt -> <MPI_Op> -> IO CInt
#ccall p8est_comm_checksum , Ptr <struct p8est> -> CUInt -> CSize -> IO CUInt

------------------------------------------------------------------------------
{- typedef struct p8est_transfer_context {
            int variable;
            int num_senders;
            int num_receivers;
            MPI_Request * recv_req;
            MPI_Request * send_req;
        } p8est_transfer_context_t; -}
#starttype struct p8est_transfer_context
#field variable , CInt
#field num_senders , CInt
#field num_receivers , CInt
#field recv_req , Ptr <MPI_Request>
#field send_req , Ptr <MPI_Request>
#stoptype
#synonym_t p8est_transfer_context_t , <struct p8est_transfer_context>
------------------------------------------------------------------------------
#ccall p8est_transfer_fixed , Ptr CLong -> Ptr CLong -> <MPI_Comm> -> CInt -> Ptr () -> Ptr () -> CSize -> IO ()
#ccall p8est_transfer_fixed_begin , Ptr CLong -> Ptr CLong -> <MPI_Comm> -> CInt -> Ptr () -> Ptr () -> CSize -> IO (Ptr <struct p8est_transfer_context>)
#ccall p8est_transfer_fixed_end , Ptr <struct p8est_transfer_context> -> IO ()
#ccall p8est_transfer_custom , Ptr CLong -> Ptr CLong -> <MPI_Comm> -> CInt -> Ptr () -> Ptr CInt -> Ptr () -> Ptr CInt -> IO ()
#ccall p8est_transfer_custom_begin , Ptr CLong -> Ptr CLong -> <MPI_Comm> -> CInt -> Ptr () -> Ptr CInt -> Ptr () -> Ptr CInt -> IO (Ptr <struct p8est_transfer_context>)
#ccall p8est_transfer_custom_end , Ptr <struct p8est_transfer_context> -> IO ()

------------------------------------------------------------------------------
#else /* !new_P8 */
------------------------------------------------------------------------------

{- typedef enum {
            P8EST_COMM_COUNT_PERTREE = 1,
            P8EST_COMM_BALANCE_FIRST_COUNT,
            P8EST_COMM_BALANCE_FIRST_LOAD,
            P8EST_COMM_BALANCE_SECOND_COUNT,
            P8EST_COMM_BALANCE_SECOND_LOAD,
            P8EST_COMM_PARTITION_GIVEN,
            P8EST_COMM_PARTITION_WEIGHTED_LOW,
            P8EST_COMM_PARTITION_WEIGHTED_HIGH,
            P8EST_COMM_PARTITION_CORRECTION,
            P8EST_COMM_GHOST_COUNT,
            P8EST_COMM_GHOST_LOAD,
            P8EST_COMM_GHOST_EXCHANGE,
            P8EST_COMM_GHOST_EXPAND_COUNT,
            P8EST_COMM_GHOST_EXPAND_LOAD,
            P8EST_COMM_GHOST_SUPPORT_COUNT,
            P8EST_COMM_GHOST_SUPPORT_LOAD,
            P8EST_COMM_GHOST_CHECKSUM,
            P8EST_COMM_NODES_QUERY,
            P8EST_COMM_NODES_REPLY,
            P8EST_COMM_SAVE,
            P8EST_COMM_LNODES_TEST,
            P8EST_COMM_LNODES_PASS,
            P8EST_COMM_LNODES_OWNED,
            P8EST_COMM_LNODES_ALL
        } p8est_comm_tag_t; -}
#integral_t p8est_comm_tag_t
#num P8EST_COMM_COUNT_PERTREE
#num P8EST_COMM_BALANCE_FIRST_COUNT
#num P8EST_COMM_BALANCE_FIRST_LOAD
#num P8EST_COMM_BALANCE_SECOND_COUNT
#num P8EST_COMM_BALANCE_SECOND_LOAD
#num P8EST_COMM_PARTITION_GIVEN
#num P8EST_COMM_PARTITION_WEIGHTED_LOW
#num P8EST_COMM_PARTITION_WEIGHTED_HIGH
#num P8EST_COMM_PARTITION_CORRECTION
#num P8EST_COMM_GHOST_COUNT
#num P8EST_COMM_GHOST_LOAD
#num P8EST_COMM_GHOST_EXCHANGE
#num P8EST_COMM_GHOST_EXPAND_COUNT
#num P8EST_COMM_GHOST_EXPAND_LOAD
#num P8EST_COMM_GHOST_SUPPORT_COUNT
#num P8EST_COMM_GHOST_SUPPORT_LOAD
#num P8EST_COMM_GHOST_CHECKSUM
#num P8EST_COMM_NODES_QUERY
#num P8EST_COMM_NODES_REPLY
#num P8EST_COMM_SAVE
#num P8EST_COMM_LNODES_TEST
#num P8EST_COMM_LNODES_PASS
#num P8EST_COMM_LNODES_OWNED
#num P8EST_COMM_LNODES_ALL

#ccall p8est_comm_count_quadrants , Ptr <struct p8est> -> IO ()
#ccall p8est_comm_global_partition , Ptr <struct p8est> -> Ptr <struct p8est_quadrant> -> IO ()
#ccall p8est_comm_count_pertree , Ptr <struct p8est> -> Ptr CLong -> IO ()
#ccall p8est_comm_is_owner , Ptr <struct p8est> -> CInt -> Ptr <struct p8est_quadrant> -> CInt -> IO CInt
#ccall p8est_comm_find_owner , Ptr <struct p8est> -> CInt -> Ptr <struct p8est_quadrant> -> CInt -> IO CInt
#ccall p8est_comm_tree_info , Ptr <struct p8est> -> CInt -> Ptr CInt -> Ptr CInt -> Ptr (Ptr <struct p8est_quadrant>) -> Ptr (Ptr <struct p8est_quadrant>) -> IO ()
#ccall p8est_comm_neighborhood_owned , Ptr <struct p8est> -> CInt -> Ptr CInt -> Ptr CInt -> Ptr <struct p8est_quadrant> -> IO CInt
#ccall p8est_comm_sync_flag , Ptr <struct p8est> -> CInt -> <MPI_Op> -> IO CInt
#ccall p8est_comm_checksum , Ptr <struct p8est> -> CUInt -> CSize -> IO CUInt


-- * From future versions of 'p8est'
------------------------------------------------------------------------------
#ccall p8est_comm_is_contained , Ptr <struct p8est> -> CInt -> Ptr <struct p8est_quadrant> -> CInt -> IO CInt

------------------------------------------------------------------------------
#endif /* new_P8 */
------------------------------------------------------------------------------
