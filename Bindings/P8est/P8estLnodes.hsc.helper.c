#include <bindings.cmacros.h>
#include <sc.h>
#include <p8est.h>
#include <p8est_lnodes.h>

BC_INLINE3(p8est_lnodes_decode, p8est_lnodes_code_t, int*, int*, int)
BC_INLINE2(p8est_lnodes_rank_array_index_int, sc_array_t*, int, p8est_lnodes_rank_t*)
BC_INLINE2(p8est_lnodes_rank_array_index, sc_array_t*, size_t, p8est_lnodes_rank_t*)
BC_INLINE2(p8est_lnodes_global_index, p8est_lnodes_t*, p4est_locidx_t, p4est_gloidx_t)
