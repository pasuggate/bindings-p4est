{-# OPTIONS_GHC -fno-warn-unused-imports #-}
#include <bindings.dsl.h>
#include "p8est_extended.h"
module Bindings.P8est.P8estExtended where
import Foreign.Ptr
#strict_import

import Bindings.SC.ScMpi
import Bindings.SC.ScContainers
import Bindings.SC.ScIo
import Bindings.P8est.P8est
import Bindings.P8est.P8estConnectivity
import Bindings.P8est.P8estGhost
import Bindings.P8est.P8estMesh
import Bindings.P8est.P8estIterate

{-- }
-- NOTE: moved to `P8est.hsc` to prevent problems with cyclic dependencies.
{- struct p8est_inspect {
    int use_balance_ranges;
    int use_balance_ranges_notify;
    int use_balance_verify;
    int balance_max_ranges;
    size_t balance_A_count_in;
    size_t balance_A_count_out;
    size_t balance_comm_sent;
    size_t balance_comm_nzpeers;
    size_t balance_B_count_in;
    size_t balance_B_count_out;
    size_t balance_zero_sends[2], balance_zero_receives[2];
    double balance_A;
    double balance_comm;
    double balance_B;
    double balance_ranges;
    double balance_notify;
    double balance_notify_allgather;
    int use_B;
}; -}
#starttype struct p8est_inspect
#field use_balance_ranges , CInt
#field use_balance_ranges_notify , CInt
#field use_balance_verify , CInt
#field balance_max_ranges , CInt
#field balance_A_count_in , CSize
#field balance_A_count_out , CSize
#field balance_comm_sent , CSize
#field balance_comm_nzpeers , CSize
#field balance_B_count_in , CSize
#field balance_B_count_out , CSize
#array_field balance_zero_sends , CSize
#array_field balance_zero_receives , CSize
#field balance_A , CDouble
#field balance_comm , CDouble
#field balance_B , CDouble
#field balance_ranges , CDouble
#field balance_notify , CDouble
#field balance_notify_allgather , CDouble
#field use_B , CInt
#stoptype
--}

#callback p8est_replace_t , Ptr <struct p8est> -> CInt -> CInt -> Ptr (Ptr <struct p8est_quadrant>) -> CInt -> Ptr (Ptr <struct p8est_quadrant>) -> IO ()

#ccall p8est_new_ext , <MPI_Comm> -> Ptr <struct p8est_connectivity> -> CInt -> CInt -> CInt -> CSize -> <p8est_init_t> -> Ptr () -> IO (Ptr <struct p8est>)
#ccall p8est_mesh_new_ext , Ptr <struct p8est> -> Ptr <p8est_ghost_t> -> CInt -> CInt -> <p8est_connect_type_t> -> IO (Ptr <p8est_mesh_t>)
#ccall p8est_refine_ext , Ptr <struct p8est> -> CInt -> CInt -> <p8est_refine_t> -> <p8est_init_t> -> <p8est_replace_t> -> IO ()
#ccall p8est_coarsen_ext , Ptr <struct p8est> -> CInt -> CInt -> <p8est_coarsen_t> -> <p8est_init_t> -> <p8est_replace_t> -> IO ()
#ccall p8est_balance_ext , Ptr <struct p8est> -> <p8est_connect_type_t> -> <p8est_init_t> -> <p8est_replace_t> -> IO ()
#ccall p8est_balance_subtree_ext , Ptr <struct p8est> -> <p8est_connect_type_t> -> CInt -> <p8est_init_t> -> <p8est_replace_t> -> IO ()
#ccall p8est_partition_ext , Ptr <struct p8est> -> CInt -> <p8est_weight_t> -> IO CLong
#ccall p8est_iterate_ext , Ptr <struct p8est> -> Ptr <p8est_ghost_t> -> Ptr () -> <p8est_iter_volume_t> -> <p8est_iter_face_t> -> <p8est_iter_corner_t> -> CInt -> IO ()
#ccall p8est_save_ext , CString -> Ptr <struct p8est> -> CInt -> CInt -> IO ()
#ccall p8est_load_ext , CString -> <MPI_Comm> -> CSize -> CInt -> CInt -> CInt -> Ptr () -> Ptr (Ptr <struct p8est_connectivity>) -> IO (Ptr <struct p8est>)
#ccall p8est_source_ext , Ptr <struct sc_io_source> -> <MPI_Comm> -> CSize -> CInt -> CInt -> CInt -> Ptr () -> Ptr (Ptr <struct p8est_connectivity>) -> IO (Ptr <struct p8est>)
