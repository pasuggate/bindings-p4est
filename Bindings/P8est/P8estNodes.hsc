{-# OPTIONS_GHC -fno-warn-unused-imports #-}
#include <bindings.dsl.h>
#include "p8est_nodes.h"
module Bindings.P8est.P8estNodes where
import Foreign.Ptr
#strict_import

import Bindings.SC.ScContainers
import Bindings.SC.ScMpi
import Bindings.P8est.P8est
import Bindings.P8est.P8estGhost


{- typedef struct p8est_indep {
            p8est_qcoord_t x, y;
            int8_t level, pad8;
            int16_t pad16;
            union p8est_indep_data {
                void * unused;
                p8est_topidx_t which_tree;
                struct {
                    p8est_topidx_t which_tree; int owner_rank;
                } piggy1;
                struct {
                    p8est_topidx_t which_tree; p8est_topidx_t from_tree;
                } piggy_unused2;
                struct {
                    p8est_topidx_t which_tree; p8est_locidx_t local_num;
                } piggy3;
            } p;
        } p8est_indep_t; -}
#starttype struct p8est_indep
#field x , CInt
#field y , CInt
#field level , CSChar
#field pad8 , CSChar
#field pad16 , CShort
-- #field p.unused , Ptr ()
#field p.piggy3.which_tree , CInt
#field p.piggy3.local_num  , CInt
-- #field p , <union p8est_indep_data>
#stoptype
#synonym_t p8est_indep_t , <struct p8est_indep>

{- typedef struct p8est_hang2 {
            p8est_qcoord_t x, y;
            int8_t level, pad8;
            int16_t pad16;
            union p8est_hang2_data {
                void * unused;
                p8est_topidx_t which_tree;
                struct {
                    p8est_topidx_t which_tree; int owner_rank;
                } piggy_unused1;
                struct {
                    p8est_topidx_t which_tree; p8est_topidx_t from_tree;
                } piggy_unused2;
                struct {
                    p8est_topidx_t which_tree; p8est_locidx_t local_num;
                } piggy_unused3;
                struct {
                    p8est_topidx_t which_tree; p8est_locidx_t depends[2];
                } piggy;
            } p;
        } p8est_hang2_t; -}
#starttype struct p8est_hang2
#field x , CInt
#field y , CInt
#field level , CSChar
#field pad8 , CSChar
#field pad16 , CShort
-- #field p.unused , Ptr ()
#field p.piggy.which_tree , CInt
#field p.piggy.depends[0] , CInt
#field p.piggy.depends[1] , CInt
-- #field p , <union p8est_hang2_data>
#stoptype
#synonym_t p8est_hang2_t , <struct p8est_hang2>

{- typedef struct p8est_nodes {
            p8est_locidx_t num_local_quadrants;
            p8est_locidx_t num_owned_indeps, num_owned_shared;
            p8est_locidx_t offset_owned_indeps;
            sc_array_t indep_nodes;
            sc_array_t face_hangings;
            p8est_locidx_t * local_nodes;
            sc_array_t shared_indeps;
            p8est_locidx_t * shared_offsets;
            int * nonlocal_ranks;
            p8est_locidx_t * global_owned_indeps;
        } p8est_nodes_t; -}
#starttype struct p8est_nodes
#field num_local_quadrants , CInt
#field num_owned_indeps , CInt
#field num_owned_shared , CInt
#field offset_owned_indeps , CInt
#field indep_nodes , <struct sc_array>
#field face_hangings , <struct sc_array>
#field local_nodes , Ptr CInt
#field shared_indeps , <struct sc_array>
#field shared_offsets , Ptr CInt
#field nonlocal_ranks , Ptr CInt
#field global_owned_indeps , Ptr CInt
#stoptype
#synonym_t p8est_nodes_t , <struct p8est_nodes>

#ccall p8est_nodes_new , Ptr <struct p8est> -> Ptr <p8est_ghost_t> -> IO (Ptr <struct p8est_nodes>)
#ccall p8est_nodes_destroy , Ptr <struct p8est_nodes> -> IO ()
#ccall p8est_nodes_is_valid , Ptr <struct p8est> -> Ptr <struct p8est_nodes> -> IO CInt
