{-# OPTIONS_GHC -fno-warn-unused-imports #-}
#include <bindings.dsl.h>
#include "p8est_geometry.h"
module Bindings.P8est.P8estGeometry where
import Foreign.Ptr
#strict_import

import Bindings.P8est.P8estConnectivity


{- typedef struct p8est_geometry p8est_geometry_t; -}
#synonym_t p8est_geometry_t , <struct p8est_geometry>

#callback p8est_geometry_X_t , Ptr <struct p8est_geometry> -> CInt -> Ptr CDouble -> Ptr CDouble -> IO ()
#callback p8est_geometry_destroy_t , Ptr <struct p8est_geometry> -> IO ()

{- struct p8est_geometry {
    const char * name;
    void * user;
    p8est_geometry_X_t X;
    p8est_geometry_destroy_t destroy;
}; -}
#starttype struct p8est_geometry
#field name , CString
#field user , Ptr ()
#field X , <p8est_geometry_X_t>
#field destroy , <p8est_geometry_destroy_t>
#stoptype

#ccall p8est_geometry_destroy , Ptr <struct p8est_geometry> -> IO ()
#ccall p8est_geometry_new_connectivity , Ptr <struct p8est_connectivity> -> IO (Ptr <struct p8est_geometry>)
