{-# OPTIONS_GHC -fno-warn-unused-imports #-}
#include <bindings.dsl.h>
#include "p8est_bits.h"
module Bindings.P8est.P8estBits where

#strict_import
import Foreign.Ptr
import Bindings.SC.Sc
import Bindings.SC.ScContainers
import Bindings.P8est.P8est
import Bindings.P8est.P8estConnectivity


#ccall p8est_quadrant_print , CInt -> Ptr <struct p8est_quadrant> -> IO ()
#ccall p8est_quadrant_is_equal , Ptr <struct p8est_quadrant> -> Ptr <struct p8est_quadrant> -> IO CInt
#ccall p8est_quadrant_overlaps , Ptr <struct p8est_quadrant> -> Ptr <struct p8est_quadrant> -> IO CInt
#ccall p8est_quadrant_is_equal_piggy , Ptr <struct p8est_quadrant> -> Ptr <struct p8est_quadrant> -> IO CInt
#ccall p8est_quadrant_compare , Ptr () -> Ptr () -> IO CInt
#ccall p8est_quadrant_disjoint , Ptr () -> Ptr () -> IO CInt
#ccall p8est_quadrant_compare_piggy , Ptr () -> Ptr () -> IO CInt
#ccall p8est_quadrant_compare_local_num , Ptr () -> Ptr () -> IO CInt
#ccall p8est_quadrant_equal_fn , Ptr () -> Ptr () -> Ptr () -> IO CInt
#ccall p8est_quadrant_hash_fn , Ptr () -> Ptr () -> IO CUInt
#ccall p8est_node_equal_piggy_fn , Ptr () -> Ptr () -> Ptr () -> IO CInt
#ccall p8est_node_hash_piggy_fn , Ptr () -> Ptr () -> IO CUInt
#ccall p8est_node_clamp_inside , Ptr <struct p8est_quadrant> -> Ptr <struct p8est_quadrant> -> IO ()
#ccall p8est_node_unclamp , Ptr <struct p8est_quadrant> -> IO ()
#ccall p8est_node_to_quadrant , Ptr <struct p8est_quadrant> -> CInt -> Ptr <struct p8est_quadrant> -> IO ()
#ccall p8est_quadrant_contains_node , Ptr <struct p8est_quadrant> -> Ptr <struct p8est_quadrant> -> IO CInt
#ccall p8est_quadrant_ancestor_id , Ptr <struct p8est_quadrant> -> CInt -> IO CInt
#ccall p8est_quadrant_child_id , Ptr <struct p8est_quadrant> -> IO CInt
#ccall p8est_quadrant_is_inside_root , Ptr <struct p8est_quadrant> -> IO CInt
#ccall p8est_quadrant_is_inside_3x3 , Ptr <struct p8est_quadrant> -> IO CInt
#ccall p8est_quadrant_is_outside_face , Ptr <struct p8est_quadrant> -> IO CInt
#ccall p8est_quadrant_is_outside_corner , Ptr <struct p8est_quadrant> -> IO CInt
#ccall p8est_quadrant_is_node , Ptr <struct p8est_quadrant> -> CInt -> IO CInt
#ccall p8est_quadrant_is_valid , Ptr <struct p8est_quadrant> -> IO CInt
#ccall p8est_quadrant_is_extended , Ptr <struct p8est_quadrant> -> IO CInt
#ccall p8est_quadrant_is_sibling , Ptr <struct p8est_quadrant> -> Ptr <struct p8est_quadrant> -> IO CInt
#ccall p8est_quadrant_is_sibling_D , Ptr <struct p8est_quadrant> -> Ptr <struct p8est_quadrant> -> IO CInt
#ccall p8est_quadrant_is_family , Ptr <struct p8est_quadrant> -> Ptr <struct p8est_quadrant> -> Ptr <struct p8est_quadrant> -> Ptr <struct p8est_quadrant> -> IO CInt
#ccall p8est_quadrant_is_familyv , Ptr (<struct p8est_quadrant>) -> IO CInt
#ccall p8est_quadrant_is_familypv , Ptr (Ptr <struct p8est_quadrant>) -> IO CInt
#ccall p8est_quadrant_is_parent , Ptr <struct p8est_quadrant> -> Ptr <struct p8est_quadrant> -> IO CInt
#ccall p8est_quadrant_is_parent_D , Ptr <struct p8est_quadrant> -> Ptr <struct p8est_quadrant> -> IO CInt
#ccall p8est_quadrant_is_ancestor , Ptr <struct p8est_quadrant> -> Ptr <struct p8est_quadrant> -> IO CInt
#ccall p8est_quadrant_is_ancestor_D , Ptr <struct p8est_quadrant> -> Ptr <struct p8est_quadrant> -> IO CInt
#ccall p8est_quadrant_is_next , Ptr <struct p8est_quadrant> -> Ptr <struct p8est_quadrant> -> IO CInt
#ccall p8est_quadrant_is_next_D , Ptr <struct p8est_quadrant> -> Ptr <struct p8est_quadrant> -> IO CInt
#ccall p8est_quadrant_overlaps_tree , Ptr <struct p8est_tree> -> Ptr <struct p8est_quadrant> -> IO CInt
#ccall p8est_quadrant_is_inside_tree , Ptr <struct p8est_tree> -> Ptr <struct p8est_quadrant> -> IO CInt
#ccall p8est_quadrant_ancestor , Ptr <struct p8est_quadrant> -> CInt -> Ptr <struct p8est_quadrant> -> IO ()
#ccall p8est_quadrant_parent , Ptr <struct p8est_quadrant> -> Ptr <struct p8est_quadrant> -> IO ()
#ccall p8est_quadrant_sibling , Ptr <struct p8est_quadrant> -> Ptr <struct p8est_quadrant> -> CInt -> IO ()


-- * Find/compute neighbours
------------------------------------------------------------------------------
#ccall p8est_quadrant_face_neighbor , Ptr <struct p8est_quadrant> -> CInt -> Ptr <struct p8est_quadrant> -> IO ()
#ccall p8est_quadrant_face_neighbor_extra , Ptr <struct p8est_quadrant> -> CInt -> CInt -> Ptr <struct p8est_quadrant> -> Ptr CInt -> Ptr <struct p8est_connectivity> -> IO CInt

#ccall p8est_quadrant_half_face_neighbors , Ptr <struct p8est_quadrant> -> CInt -> Ptr (<struct p8est_quadrant>) -> Ptr (<struct p8est_quadrant>) -> IO ()
#ccall p8est_quadrant_all_face_neighbors , Ptr <struct p8est_quadrant> -> CInt -> Ptr (<struct p8est_quadrant>) -> IO ()
#ccall p8est_quadrant_corner_neighbor , Ptr <struct p8est_quadrant> -> CInt -> Ptr <struct p8est_quadrant> -> IO ()
#ccall p8est_quadrant_corner_neighbor_extra , Ptr <struct p8est_quadrant> -> CInt -> CInt -> Ptr <struct sc_array> -> Ptr <struct sc_array> -> Ptr <struct sc_array> -> Ptr <struct p8est_connectivity> -> IO ()
#ccall p8est_quadrant_half_corner_neighbor , Ptr <struct p8est_quadrant> -> CInt -> Ptr <struct p8est_quadrant> -> IO ()
#ccall p8est_quadrant_corner_node , Ptr <struct p8est_quadrant> -> CInt -> Ptr <struct p8est_quadrant> -> IO ()


#ccall p8est_quadrant_children , Ptr <struct p8est_quadrant> -> Ptr <struct p8est_quadrant> -> Ptr <struct p8est_quadrant> -> Ptr <struct p8est_quadrant> -> Ptr <struct p8est_quadrant> -> IO ()
#ccall p8est_quadrant_childrenv , Ptr <struct p8est_quadrant> -> Ptr (<struct p8est_quadrant>) -> IO ()
#ccall p8est_quadrant_childrenpv , Ptr <struct p8est_quadrant> -> Ptr (Ptr <struct p8est_quadrant>) -> IO ()
#ccall p8est_quadrant_first_descendant , Ptr <struct p8est_quadrant> -> Ptr <struct p8est_quadrant> -> CInt -> IO ()
#ccall p8est_quadrant_last_descendant , Ptr <struct p8est_quadrant> -> Ptr <struct p8est_quadrant> -> CInt -> IO ()
#ccall p8est_quadrant_corner_descendant , Ptr <struct p8est_quadrant> -> Ptr <struct p8est_quadrant> -> CInt -> CInt -> IO ()
#ccall p8est_nearest_common_ancestor , Ptr <struct p8est_quadrant> -> Ptr <struct p8est_quadrant> -> Ptr <struct p8est_quadrant> -> IO ()
#ccall p8est_nearest_common_ancestor_D , Ptr <struct p8est_quadrant> -> Ptr <struct p8est_quadrant> -> Ptr <struct p8est_quadrant> -> IO ()
#ccall p8est_quadrant_touches_corner , Ptr <struct p8est_quadrant> -> CInt -> CInt -> IO CInt
#ccall p8est_quadrant_shift_corner , Ptr <struct p8est_quadrant> -> Ptr <struct p8est_quadrant> -> CInt -> IO ()

-- ** Face & corner transforms
------------------------------------------------------------------------------
#ccall p8est_quadrant_transform_face , Ptr <struct p8est_quadrant> -> Ptr <struct p8est_quadrant> -> Ptr CInt -> IO ()
#ccall p8est_quadrant_transform_corner , Ptr <struct p8est_quadrant> -> CInt -> CInt -> IO ()


-- * Z-indices
------------------------------------------------------------------------------
#ccall p8est_quadrant_linear_id , Ptr <struct p8est_quadrant> -> CInt -> IO CULong
#ccall p8est_quadrant_set_morton , Ptr <struct p8est_quadrant> -> CInt -> CULong -> IO ()


-- * From future versions of 'p8est'
------------------------------------------------------------------------------
#ccall p8est_quadrant_child , Ptr <struct p8est_quadrant> -> Ptr <struct p8est_quadrant> -> CInt -> IO ()
