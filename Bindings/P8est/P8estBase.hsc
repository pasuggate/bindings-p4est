{-# OPTIONS_GHC -fno-warn-unused-imports #-}
#include <bindings.dsl.h>
#include "p8est_base.h"
{- for old versions of 'p8est' -}
#include "p8est_communication.h"
module Bindings.P8est.P8estBase where
#strict_import
import Foreign.Ptr

import Bindings.SC.Sc


{- typedef int32_t p8est_qcoord_t; -}
#synonym_t p8est_qcoord_t , CInt
{- typedef int32_t p8est_topidx_t; -}
#synonym_t p8est_topidx_t , CInt
{- typedef int32_t p8est_locidx_t; -}
#synonym_t p8est_locidx_t , CInt
{- typedef int64_t p8est_gloidx_t; -}
#synonym_t p8est_gloidx_t , CLong

{- MPI tags used by 'p8est' -}
#integral_t p8est_comm_tag_t
{- #num P8EST_COMM_TAG_FIRST -}
#num P8EST_COMM_COUNT_PERTREE
{- #num P8EST_COMM_TAG_LAST -}
#num P8EST_COMM_LNODES_ALL

#ccall P8EST_GLOBAL_LOGF , CInt -> CString -> IO ()
#ccall P8EST_LOGF , CInt -> CString -> IO ()
#ccall P8EST_GLOBAL_TRACEF , CString -> IO ()
#ccall P8EST_GLOBAL_LDEBUGF , CString -> IO ()
#ccall P8EST_GLOBAL_VERBOSEF , CString -> IO ()
#ccall P8EST_GLOBAL_INFOF , CString -> IO ()
#ccall P8EST_GLOBAL_STATISTICSF , CString -> IO ()
#ccall P8EST_GLOBAL_PRODUCTIONF , CString -> IO ()
#ccall P8EST_GLOBAL_ESSENTIALF , CString -> IO ()
#ccall P8EST_GLOBAL_LERRORF , CString -> IO ()
#ccall P8EST_TRACEF , CString -> IO ()
#ccall P8EST_LDEBUGF , CString -> IO ()
#ccall P8EST_VERBOSEF , CString -> IO ()
#ccall P8EST_INFOF , CString -> IO ()
#ccall P8EST_STATISTICSF , CString -> IO ()
#ccall P8EST_PRODUCTIONF , CString -> IO ()
#ccall P8EST_ESSENTIALF , CString -> IO ()
#ccall P8EST_LERRORF , CString -> IO ()

#globalvar p8est_package_id , CInt

#cinline p8est_log_indent_push , IO ()
#cinline p8est_log_indent_pop , IO ()

#ccall p8est_init , <sc_log_handler_t> -> CInt -> IO ()

#cinline p8est_topidx_hash2 , Ptr CInt -> IO CUInt
#cinline p8est_topidx_hash3 , Ptr CInt -> IO CUInt
#cinline p8est_topidx_hash4 , Ptr CInt -> IO CUInt
#cinline p8est_topidx_is_sorted , Ptr CInt -> CInt -> IO CInt
#cinline p8est_topidx_bsort , Ptr CInt -> CInt -> IO ()
#cinline p8est_partition_cut_uint64 , CULong -> CInt -> CInt -> IO CULong
#cinline p8est_partition_cut_gloidx , CLong -> CInt -> CInt -> IO CLong
