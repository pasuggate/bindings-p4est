#include <bindings.cmacros.h>
#include <stdint.h>
#include <p8est.h>

BC_INLINE0VOID(p8est_log_indent_push)
BC_INLINE0VOID(p8est_log_indent_pop)
BC_INLINE1(p8est_topidx_hash2, const p8est_topidx_t*, unsigned)
BC_INLINE1(p8est_topidx_hash3, const p8est_topidx_t*, unsigned)
BC_INLINE1(p8est_topidx_hash4, const p8est_topidx_t*, unsigned)
BC_INLINE2(p8est_topidx_is_sorted, p8est_topidx_t*, int, int)
BC_INLINE2VOID(p8est_topidx_bsort, p8est_topidx_t*, int)
BC_INLINE3(p8est_partition_cut_uint64, uint64_t, int, int, uint64_t)
BC_INLINE3(p8est_partition_cut_gloidx, p8est_gloidx_t, int, int, p8est_gloidx_t)
