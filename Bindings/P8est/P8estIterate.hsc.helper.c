#include <bindings.cmacros.h>
#include <sc.h>
#include <p8est.h>
#include <p8est_iterate.h>

BC_INLINE2(p8est_iter_cside_array_index_int, sc_array_t*, int, p8est_iter_corner_side_t*)
BC_INLINE2(p8est_iter_cside_array_index, sc_array_t*, size_t, p8est_iter_corner_side_t*)
BC_INLINE2(p8est_iter_fside_array_index_int, sc_array_t*, int, p8est_iter_face_side_t*)
BC_INLINE2(p8est_iter_fside_array_index, sc_array_t*, size_t, p8est_iter_face_side_t*)
