{-# OPTIONS_GHC -fno-warn-unused-imports #-}
#include <bindings.dsl.h>
#include "p8est_connectivity.h"
module Bindings.P8est.P8estConnectivity where
import Foreign.Ptr
#strict_import


import Bindings.SC.Sc
import Bindings.SC.ScContainers
import Bindings.SC.ScIo


{- typedef enum {
            P8EST_CONNECT_FACE = 31,
            P8EST_CONNECT_EDGE = 32,
            P8EST_CONNECT_CORNER = 33,
            P8EST_CONNECT_FULL = P8EST_CONNECT_CORNER
        } p8est_connect_type_t; -}
#integral_t p8est_connect_type_t
#num P8EST_CONNECT_FACE
#num P8EST_CONNECT_EDGE
#num P8EST_CONNECT_CORNER
#num P8EST_CONNECT_FULL

{- typedef enum {
            P8EST_CONN_ENCODE_NONE = SC_IO_ENCODE_NONE, P8EST_CONN_ENCODE_LAST
        } p8est_connectivity_encode_t; -}
#integral_t p8est_connectivity_encode_t
#num P8EST_CONN_ENCODE_NONE
#num P8EST_CONN_ENCODE_LAST

#ccall p8est_connect_type_int , <p8est_connect_type_t> -> IO CInt
#ccall p8est_connect_type_string , <p8est_connect_type_t> -> IO CString

------------------------------------------------------------------------------
{- typedef struct p8est_connectivity {
            p4est_topidx_t num_vertices;
            p4est_topidx_t num_trees;
            p4est_topidx_t num_edges;
            p4est_topidx_t num_corners;
            double * vertices;
            p4est_topidx_t * tree_to_vertex;
            size_t tree_attr_bytes;
            char * tree_to_attr;
            p4est_topidx_t * tree_to_tree;
            int8_t * tree_to_face;
            p4est_topidx_t * tree_to_edge;
            p4est_topidx_t * ett_offset;
            p4est_topidx_t * edge_to_tree;
            int8_t * edge_to_edge;
            p4est_topidx_t * tree_to_corner;
            p4est_topidx_t * ctt_offset;
            p4est_topidx_t * corner_to_tree;
            int8_t * corner_to_corner;
        } p8est_connectivity_t; -}
#starttype struct p8est_connectivity
#field num_vertices , CInt
#field num_trees , CInt
#field num_edges , CInt
#field num_corners , CInt
#field vertices , Ptr CDouble
#field tree_to_vertex , Ptr CInt
#field tree_attr_bytes , CSize
#field tree_to_attr , CString
#field tree_to_tree , Ptr CInt
#field tree_to_face , Ptr CSChar
#field tree_to_edge , Ptr CInt
#field ett_offset , Ptr CInt
#field edge_to_tree , Ptr CInt
#field edge_to_edge , Ptr CSChar
#field tree_to_corner , Ptr CInt
#field ctt_offset , Ptr CInt
#field corner_to_tree , Ptr CInt
#field corner_to_corner , Ptr CSChar
#stoptype
#synonym_t p8est_connectivity_t , <struct p8est_connectivity>
------------------------------------------------------------------------------
#ccall p8est_connectivity_memory_used , Ptr <struct p8est_connectivity> -> IO CSize
------------------------------------------------------------------------------
{- typedef struct {
            p4est_topidx_t ntree; int8_t nedge, naxis[3], nflip, corners;
        } p8est_edge_transform_t; -}
#starttype p8est_edge_transform_t
#field ntree , CInt
#field nedge , CSChar
#field naxis , CSChar
#field nflip , CSChar
#field corners , CSChar
#stoptype
------------------------------------------------------------------------------
{- typedef struct {
            int8_t iedge; sc_array_t edge_transforms;
        } p8est_edge_info_t; -}
#starttype p8est_edge_info_t
#field iedge , CSChar
#field edge_transforms , <struct sc_array>
#stoptype
------------------------------------------------------------------------------

------------------------------------------------------------------------------
{- typedef struct {
            p8est_topidx_t ntree; int8_t ncorner;
        } p8est_corner_transform_t; -}
#starttype p8est_corner_transform_t
#field ntree , CInt
#field ncorner , CSChar
#stoptype
------------------------------------------------------------------------------
{- typedef struct {
            p8est_topidx_t icorner; sc_array_t corner_transforms;
        } p8est_corner_info_t; -}
#starttype p8est_corner_info_t
#field icorner , CInt
#field corner_transforms , <struct sc_array>
#stoptype
------------------------------------------------------------------------------

{-
#globalarray p8est_face_corners , CInt
#globalarray p8est_face_edges , CInt
#globalarray p8est_face_dual , CInt
#globalarray p8est_face_permutations , CInt
#globalarray p8est_face_permutation_sets , CInt
#globalarray p8est_face_permutation_refs , CInt
#globalarray p8est_face_edge_permutations , CInt
#globalarray p8est_face_edge_permutation_sets , CInt
#globalarray p8est_edge_faces , CInt
#globalarray p8est_edge_corners , CInt
#globalarray p8est_edge_edge_corners , CInt
#globalarray p8est_edge_face_corners , CInt
#globalarray p8est_edge_face_edges , CInt
#globalarray p8est_corner_faces , CInt
#globalarray p8est_corner_edges , CInt
#globalarray p8est_corner_face_corners , CInt
#globalarray p8est_corner_edge_corners , CInt
#globalarray p8est_child_edge_faces , CInt
#globalarray p8est_child_corner_faces , CInt
#globalarray p8est_child_corner_edges , CInt
-}

#ccall p8est_connectivity_face_neighbor_corner_set , CInt -> CInt -> CInt -> CInt -> IO CInt
#ccall p8est_connectivity_face_neighbor_face_corner , CInt -> CInt -> CInt -> CInt -> IO CInt
#ccall p8est_connectivity_face_neighbor_corner , CInt -> CInt -> CInt -> CInt -> IO CInt
#ccall p8est_connectivity_face_neighbor_face_edge , CInt -> CInt -> CInt -> CInt -> IO CInt
#ccall p8est_connectivity_face_neighbor_edge , CInt -> CInt -> CInt -> CInt -> IO CInt
#ccall p8est_connectivity_edge_neighbor_edge_corner , CInt -> CInt -> IO CInt
#ccall p8est_connectivity_edge_neighbor_corner , CInt -> CInt -> CInt -> CInt -> IO CInt
#ccall p8est_connectivity_new , CInt -> CInt -> CInt -> CInt -> CInt -> CInt -> IO (Ptr <struct p8est_connectivity>)
#ccall p8est_connectivity_new_copy , CInt -> CInt -> CInt -> CInt -> Ptr CDouble -> Ptr CInt -> Ptr CInt -> Ptr CSChar -> Ptr CInt -> Ptr CInt -> Ptr CInt -> Ptr CSChar -> Ptr CInt -> Ptr CInt -> Ptr CInt -> Ptr CSChar -> IO (Ptr <struct p8est_connectivity>)
#ccall p8est_connectivity_bcast , Ptr <struct p8est_connectivity> -> CInt -> <MPI_Comm> -> IO (Ptr <struct p8est_connectivity>)
#ccall p8est_connectivity_destroy , Ptr <struct p8est_connectivity> -> IO ()
#ccall p8est_connectivity_set_attr , Ptr <struct p8est_connectivity> -> CSize -> IO ()
#ccall p8est_connectivity_is_valid , Ptr <struct p8est_connectivity> -> IO CInt
#ccall p8est_connectivity_is_equal , Ptr <struct p8est_connectivity> -> Ptr <struct p8est_connectivity> -> IO CInt
#ccall p8est_connectivity_sink , Ptr <struct p8est_connectivity> -> Ptr <struct sc_io_sink> -> IO CInt
#ccall p8est_connectivity_deflate , Ptr <struct p8est_connectivity> -> <p8est_connectivity_encode_t> -> IO (Ptr <struct sc_array>)
#ccall p8est_connectivity_save , CString -> Ptr <struct p8est_connectivity> -> IO CInt
#ccall p8est_connectivity_source , Ptr <struct sc_io_source> -> IO (Ptr <struct p8est_connectivity>)
#ccall p8est_connectivity_inflate , Ptr <struct sc_array> -> IO (Ptr <struct p8est_connectivity>)
#ccall p8est_connectivity_load , CString -> Ptr CSize -> IO (Ptr <struct p8est_connectivity>)
------------------------------------------------------------------------------
#ccall p8est_connectivity_new_unitcube , IO (Ptr <struct p8est_connectivity>)
#ccall p8est_connectivity_new_periodic , IO (Ptr <struct p8est_connectivity>)
#ccall p8est_connectivity_new_rotwrap , IO (Ptr <struct p8est_connectivity>)
#ccall p8est_connectivity_new_twocubes , IO (Ptr <struct p8est_connectivity>)
#ccall p8est_connectivity_new_twotrees , CInt -> CInt -> CInt -> IO (Ptr <struct p8est_connectivity>)
#ccall p8est_connectivity_new_twowrap , IO (Ptr <struct p8est_connectivity>)
#ccall p8est_connectivity_new_rotcubes , IO (Ptr <struct p8est_connectivity>)
#ccall p8est_connectivity_new_brick , CInt -> CInt -> CInt -> CInt -> CInt -> CInt -> IO (Ptr <struct p8est_connectivity>)
#ccall p8est_connectivity_new_shell , IO (Ptr <struct p8est_connectivity>)
#ccall p8est_connectivity_new_sphere , IO (Ptr <struct p8est_connectivity>)
#ccall p8est_connectivity_new_byname , CString -> IO (Ptr <struct p8est_connectivity>)
------------------------------------------------------------------------------
#ccall p8est_connectivity_refine , Ptr <struct p8est_connectivity> -> CInt -> IO (Ptr <struct p8est_connectivity>)
------------------------------------------------------------------------------
#ccall p8est_expand_face_transform , CInt -> CInt -> Ptr CInt -> IO ()
#ccall p8est_find_face_transform , Ptr <struct p8est_connectivity> -> CInt -> CInt -> Ptr CInt -> IO CInt
#ccall p8est_find_edge_transform , Ptr <struct p8est_connectivity> -> CInt -> CInt -> Ptr <p8est_edge_info_t> -> IO ()
#ccall p8est_find_corner_transform , Ptr <struct p8est_connectivity> -> CInt -> CInt -> Ptr <p8est_corner_info_t> -> IO ()
------------------------------------------------------------------------------
#ccall p8est_connectivity_complete , Ptr <struct p8est_connectivity> -> IO ()
#ccall p8est_connectivity_reduce , Ptr <struct p8est_connectivity> -> IO ()
#ccall p8est_connectivity_permute , Ptr <struct p8est_connectivity> -> Ptr <struct sc_array> -> CInt -> IO ()
#ccall p8est_connectivity_join_faces , Ptr <struct p8est_connectivity> -> CInt -> CInt -> CInt -> CInt -> CInt -> IO ()
#ccall p8est_connectivity_is_equivalent , Ptr <struct p8est_connectivity> -> Ptr <struct p8est_connectivity> -> IO CInt
------------------------------------------------------------------------------
#cinline p8est_edge_array_index , Ptr <struct sc_array> -> CSize -> IO (Ptr <p8est_edge_transform_t>)
#cinline p8est_corner_array_index , Ptr <struct sc_array> -> CSize -> IO (Ptr <p8est_corner_transform_t>)
------------------------------------------------------------------------------
#ccall p8est_connectivity_read_inp_stream , Ptr <struct _IO_FILE> -> Ptr CInt -> Ptr CInt -> Ptr CDouble -> Ptr CInt -> IO CInt
#ccall p8est_connectivity_read_inp , CString -> IO (Ptr <struct p8est_connectivity>)

------------------------------------------------------------------------------
#if 0 /* old_P8 */
------------------------------------------------------------------------------
#ccall p8est_connectivity_face_neighbor_corner_orientation , CInt -> CInt -> CInt -> CInt -> IO CInt
#ccall p8est_connectivity_new , CInt -> CInt -> CInt -> CInt -> IO (Ptr <struct p8est_connectivity>)
#ccall p8est_connectivity_new_copy , CInt -> CInt -> CInt -> Ptr CDouble -> Ptr CInt -> Ptr CInt -> Ptr CSChar -> Ptr CInt -> Ptr CInt -> Ptr CInt -> Ptr CSChar -> IO (Ptr <struct p8est_connectivity>)
#ccall p8est_connectivity_destroy , Ptr <struct p8est_connectivity> -> IO ()
#ccall p8est_connectivity_set_attr , Ptr <struct p8est_connectivity> -> CSize -> IO ()
#ccall p8est_connectivity_is_valid , Ptr <struct p8est_connectivity> -> IO CInt
#ccall p8est_connectivity_is_equal , Ptr <struct p8est_connectivity> -> Ptr <struct p8est_connectivity> -> IO CInt
#ccall p8est_connectivity_sink , Ptr <struct p8est_connectivity> -> Ptr <struct sc_io_sink> -> IO CInt
#ccall p8est_connectivity_deflate , Ptr <struct p8est_connectivity> -> <p8est_connectivity_encode_t> -> IO (Ptr <struct sc_array>)
#ccall p8est_connectivity_save , CString -> Ptr <struct p8est_connectivity> -> IO CInt
#ccall p8est_connectivity_source , Ptr <struct sc_io_source> -> IO (Ptr <struct p8est_connectivity>)
#ccall p8est_connectivity_inflate , Ptr <struct sc_array> -> IO (Ptr <struct p8est_connectivity>)
#ccall p8est_connectivity_load , CString -> Ptr CSize -> IO (Ptr <struct p8est_connectivity>)
#ccall p8est_connectivity_new_unitcube , IO (Ptr <struct p8est_connectivity>)
{-
#ccall p8est_connectivity_new_unitsquare , IO (Ptr <struct p8est_connectivity>)
#ccall p8est_connectivity_new_periodic , IO (Ptr <struct p8est_connectivity>)
#ccall p8est_connectivity_new_rotwrap , IO (Ptr <struct p8est_connectivity>)
#ccall p8est_connectivity_new_corner , IO (Ptr <struct p8est_connectivity>)
#ccall p8est_connectivity_new_pillow , IO (Ptr <struct p8est_connectivity>)
#ccall p8est_connectivity_new_moebius , IO (Ptr <struct p8est_connectivity>)
#ccall p8est_connectivity_new_star , IO (Ptr <struct p8est_connectivity>)
#ccall p8est_connectivity_new_cubed , IO (Ptr <struct p8est_connectivity>)
#ccall p8est_connectivity_new_disk , IO (Ptr <struct p8est_connectivity>)
-}
#ccall p8est_connectivity_new_brick , CInt -> CInt -> CInt -> CInt -> CInt -> CInt -> IO (Ptr <struct p8est_connectivity>)
#ccall p8est_connectivity_new_byname , CString -> IO (Ptr <struct p8est_connectivity>)


-- * Face & corner transforms, between trees
------------------------------------------------------------------------------
#num P8EST_FTRANSFORM

#ccall p8est_expand_face_transform , CInt -> CInt -> Ptr CInt -> IO ()
#ccall p8est_find_face_transform , Ptr <struct p8est_connectivity> -> CInt -> CInt -> Ptr CInt -> IO CInt
#ccall p8est_find_corner_transform , Ptr <struct p8est_connectivity> -> CInt -> CInt -> Ptr <p8est_corner_info_t> -> IO ()


#ccall p8est_connectivity_complete , Ptr <struct p8est_connectivity> -> IO ()
#ccall p8est_connectivity_reduce , Ptr <struct p8est_connectivity> -> IO ()
#ccall p8est_connectivity_permute , Ptr <struct p8est_connectivity> -> Ptr <struct sc_array> -> CInt -> IO ()
#ccall p8est_connectivity_join_faces , Ptr <struct p8est_connectivity> -> CInt -> CInt -> CInt -> CInt -> CInt -> IO ()
#ccall p8est_connectivity_is_equivalent , Ptr <struct p8est_connectivity> -> Ptr <struct p8est_connectivity> -> IO CInt

#cinline p8est_corner_array_index , Ptr <struct sc_array> -> CSize -> IO (Ptr <p8est_corner_transform_t>)
#ccall p8est_connectivity_read_inp_stream , Ptr <struct _IO_FILE> -> Ptr CInt -> Ptr CInt -> Ptr CDouble -> Ptr CInt -> IO CInt
#ccall p8est_connectivity_read_inp , CString -> IO (Ptr <struct p8est_connectivity>)
------------------------------------------------------------------------------
#endif /* old_P8 */
------------------------------------------------------------------------------
