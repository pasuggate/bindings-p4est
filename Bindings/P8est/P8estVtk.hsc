{-# OPTIONS_GHC -fno-warn-unused-imports #-}
#include <bindings.dsl.h>
#include "p8est_vtk.h"
module Bindings.P8est.P8estVtk where
import Foreign.Ptr
#strict_import

import Bindings.P8est.P8est
import Bindings.P8est.P8estGeometry


------------------------------------------------------------------------------
#if 1 /* new_P8 */
------------------------------------------------------------------------------
{- typedef struct p8est_vtk_context p8est_vtk_context_t; -}
#opaque_t struct p8est_vtk_context
#synonym_t p8est_vtk_context_t , <struct p8est_vtk_context>
------------------------------------------------------------------------------
#ccall p8est_vtk_write_file , Ptr <struct p8est> -> Ptr <struct p8est_geometry> -> CString -> IO ()
#ccall p8est_vtk_context_new , Ptr <struct p8est> -> CString -> IO (Ptr <struct p8est_vtk_context>)
#ccall p8est_vtk_context_set_geom , Ptr <struct p8est_vtk_context> -> Ptr <struct p8est_geometry> -> IO ()
#ccall p8est_vtk_context_set_scale , Ptr <struct p8est_vtk_context> -> CDouble -> IO ()
#ccall p8est_vtk_context_set_continuous , Ptr <struct p8est_vtk_context> -> CInt -> IO ()
#ccall p8est_vtk_context_destroy , Ptr <struct p8est_vtk_context> -> IO ()
#ccall p8est_vtk_write_header , Ptr <struct p8est_vtk_context> -> IO (Ptr <struct p8est_vtk_context>)
-- #ccall p8est_vtk_write_cell_dataf , Ptr <struct p8est_vtk_context> -> CInt -> CInt -> CInt -> CInt -> CInt -> CInt -> IO (Ptr <struct p8est_vtk_context>)
-- #ccall p8est_vtk_write_point_dataf , Ptr <struct p8est_vtk_context> -> CInt -> CInt -> IO (Ptr <struct p8est_vtk_context>)
#ccall p8est_vtk_write_footer , Ptr <struct p8est_vtk_context> -> IO CInt

------------------------------------------------------------------------------
#else /* !new_P8 */
------------------------------------------------------------------------------

#ccall p8est_vtk_write_file , Ptr <struct p8est> -> Ptr <struct p8est_geometry> -> CString -> IO ()
#ccall p8est_vtk_write_all , Ptr <struct p8est> -> Ptr <struct p8est_geometry> -> CDouble -> CInt -> CInt -> CInt -> CInt -> CInt -> CInt -> CString -> IO ()
#ccall p8est_vtk_write_header , Ptr <struct p8est> -> Ptr <struct p8est_geometry> -> CDouble -> CInt -> CInt -> CInt -> CInt -> CString -> CString -> CString -> IO CInt
#ccall p8est_vtk_write_point_scalar , Ptr <struct p8est> -> Ptr <struct p8est_geometry> -> CString -> CString -> Ptr CDouble -> IO CInt
#ccall p8est_vtk_write_point_vector , Ptr <struct p8est> -> Ptr <struct p8est_geometry> -> CString -> CString -> Ptr CDouble -> IO CInt
#ccall p8est_vtk_write_footer , Ptr <struct p8est> -> CString -> IO CInt

------------------------------------------------------------------------------
#endif /* new_P8 */
------------------------------------------------------------------------------
