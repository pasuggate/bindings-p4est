{-# OPTIONS_GHC -fno-warn-unused-imports #-}
#include <bindings.dsl.h>
#include "p8est_iterate.h"
module Bindings.P8est.P8estIterate where
import Foreign.Ptr
#strict_import


import Bindings.SC.ScContainers
import Bindings.P8est.P8est
import Bindings.P8est.P8estGhost


{- typedef struct p8est_iter_volume_info {
            p8est_t * p8est;
            p8est_ghost_t * ghost_layer;
            p8est_quadrant_t * quad;
            p8est_locidx_t quadid;
            p8est_topidx_t treeid;
        } p8est_iter_volume_info_t; -}
#starttype struct p8est_iter_volume_info
#field p4est , Ptr <struct p8est>
#field ghost_layer , Ptr <p8est_ghost_t>
#field quad , Ptr <struct p8est_quadrant>
#field quadid , CInt
#field treeid , CInt
#stoptype
#synonym_t p8est_iter_volume_info_t , <struct p8est_iter_volume_info>
#callback p8est_iter_volume_t , Ptr <struct p8est_iter_volume_info> -> Ptr () -> IO ()

{- typedef struct p8est_iter_face_side {
            p8est_topidx_t treeid;
            int8_t face;
            int8_t is_hanging;
            union p8est_iter_face_side_data {
                struct {
                    int8_t is_ghost; p8est_quadrant_t * quad; p8est_locidx_t quadid;
                } full;
                struct {
                    int8_t is_ghost[2];
                    p8est_quadrant_t * quad[2];
                    p8est_locidx_t quadid[2];
                } hanging;
            } is;
        } p8est_iter_face_side_t; -}
#starttype struct p8est_iter_face_side
#field treeid , CInt
#field face , CSChar
#field is_hanging , CSChar
-- --->
-- #field is , <union p8est_iter_face_side_data>
-- <---
#stoptype

#synonym_t p8est_iter_face_side_t , <struct p8est_iter_face_side>
{- typedef struct p8est_iter_face_info {
            p8est_t * p8est;
            p8est_ghost_t * ghost_layer;
            int8_t orientation;
            int8_t tree_boundary;
            sc_array_t sides;
        } p8est_iter_face_info_t; -}
#starttype struct p8est_iter_face_info
#field p4est , Ptr <struct p8est>
#field ghost_layer , Ptr <p8est_ghost_t>
#field orientation , CSChar
#field tree_boundary , CSChar
#field sides , <struct sc_array>
#stoptype

#synonym_t p8est_iter_face_info_t , <struct p8est_iter_face_info>
#callback p8est_iter_face_t , Ptr <struct p8est_iter_face_info> -> Ptr () -> IO ()
{- typedef struct p8est_iter_corner_side {
            p8est_topidx_t treeid;
            int8_t corner;
            int8_t is_ghost;
            p8est_quadrant_t * quad;
            p8est_locidx_t quadid;
            int8_t faces[2];
        } p8est_iter_corner_side_t; -}
#starttype struct p8est_iter_corner_side
#field treeid , CInt
#field corner , CSChar
#field is_ghost , CSChar
#field quad , Ptr <struct p8est_quadrant>
#field quadid , CInt
#array_field faces , CSChar
#stoptype

#synonym_t p8est_iter_corner_side_t , <struct p8est_iter_corner_side>
{- typedef struct p8est_iter_corner_info {
            p8est_t * p8est;
            p8est_ghost_t * ghost_layer;
            int8_t tree_boundary;
            sc_array_t sides;
        } p8est_iter_corner_info_t; -}
#starttype struct p8est_iter_corner_info
#field p4est , Ptr <struct p8est>
#field ghost_layer , Ptr <p8est_ghost_t>
#field tree_boundary , CSChar
#field sides , <struct sc_array>
#stoptype

#synonym_t p8est_iter_corner_info_t , <struct p8est_iter_corner_info>

#callback p8est_iter_corner_t , Ptr <struct p8est_iter_corner_info> -> Ptr () -> IO ()

#ccall p8est_iterate , Ptr <struct p8est> -> Ptr <p8est_ghost_t> -> Ptr () -> <p8est_iter_volume_t> -> <p8est_iter_face_t> -> <p8est_iter_corner_t> -> IO ()
#cinline p8est_iter_cside_array_index_int , Ptr <struct sc_array> -> CInt -> IO (Ptr <struct p8est_iter_corner_side>)
#cinline p8est_iter_cside_array_index , Ptr <struct sc_array> -> CSize -> IO (Ptr <struct p8est_iter_corner_side>)
#cinline p8est_iter_fside_array_index_int , Ptr <struct sc_array> -> CInt -> IO (Ptr <struct p8est_iter_face_side>)
#cinline p8est_iter_fside_array_index , Ptr <struct sc_array> -> CSize -> IO (Ptr <struct p8est_iter_face_side>)
