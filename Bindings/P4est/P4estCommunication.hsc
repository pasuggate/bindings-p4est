{-# OPTIONS_GHC -fno-warn-unused-imports #-}
#include <bindings.dsl.h>
#include "p4est_communication.h"
module Bindings.P4est.P4estCommunication where
import Foreign.Ptr
#strict_import

import Bindings.SC.ScMpi
import Bindings.P4est.P4est


{- typedef enum {
            P4EST_COMM_COUNT_PERTREE = 1,
            P4EST_COMM_BALANCE_FIRST_COUNT,
            P4EST_COMM_BALANCE_FIRST_LOAD,
            P4EST_COMM_BALANCE_SECOND_COUNT,
            P4EST_COMM_BALANCE_SECOND_LOAD,
            P4EST_COMM_PARTITION_GIVEN,
            P4EST_COMM_PARTITION_WEIGHTED_LOW,
            P4EST_COMM_PARTITION_WEIGHTED_HIGH,
            P4EST_COMM_PARTITION_CORRECTION,
            P4EST_COMM_GHOST_COUNT,
            P4EST_COMM_GHOST_LOAD,
            P4EST_COMM_GHOST_EXCHANGE,
            P4EST_COMM_GHOST_EXPAND_COUNT,
            P4EST_COMM_GHOST_EXPAND_LOAD,
            P4EST_COMM_GHOST_SUPPORT_COUNT,
            P4EST_COMM_GHOST_SUPPORT_LOAD,
            P4EST_COMM_GHOST_CHECKSUM,
            P4EST_COMM_NODES_QUERY,
            P4EST_COMM_NODES_REPLY,
            P4EST_COMM_SAVE,
            P4EST_COMM_LNODES_TEST,
            P4EST_COMM_LNODES_PASS,
            P4EST_COMM_LNODES_OWNED,
            P4EST_COMM_LNODES_ALL
        } p4est_comm_tag_t; -}
#integral_t p4est_comm_tag_t
#num P4EST_COMM_COUNT_PERTREE
#num P4EST_COMM_BALANCE_FIRST_COUNT
#num P4EST_COMM_BALANCE_FIRST_LOAD
#num P4EST_COMM_BALANCE_SECOND_COUNT
#num P4EST_COMM_BALANCE_SECOND_LOAD
#num P4EST_COMM_PARTITION_GIVEN
#num P4EST_COMM_PARTITION_WEIGHTED_LOW
#num P4EST_COMM_PARTITION_WEIGHTED_HIGH
#num P4EST_COMM_PARTITION_CORRECTION
#num P4EST_COMM_GHOST_COUNT
#num P4EST_COMM_GHOST_LOAD
#num P4EST_COMM_GHOST_EXCHANGE
#num P4EST_COMM_GHOST_EXPAND_COUNT
#num P4EST_COMM_GHOST_EXPAND_LOAD
#num P4EST_COMM_GHOST_SUPPORT_COUNT
#num P4EST_COMM_GHOST_SUPPORT_LOAD
#num P4EST_COMM_GHOST_CHECKSUM
#num P4EST_COMM_NODES_QUERY
#num P4EST_COMM_NODES_REPLY
#num P4EST_COMM_SAVE
#num P4EST_COMM_LNODES_TEST
#num P4EST_COMM_LNODES_PASS
#num P4EST_COMM_LNODES_OWNED
#num P4EST_COMM_LNODES_ALL

#ccall p4est_comm_count_quadrants , Ptr <struct p4est> -> IO ()
#ccall p4est_comm_global_partition , Ptr <struct p4est> -> Ptr <struct p4est_quadrant> -> IO ()
#ccall p4est_comm_count_pertree , Ptr <struct p4est> -> Ptr CLong -> IO ()
#ccall p4est_comm_is_owner , Ptr <struct p4est> -> CInt -> Ptr <struct p4est_quadrant> -> CInt -> IO CInt
#ccall p4est_comm_find_owner , Ptr <struct p4est> -> CInt -> Ptr <struct p4est_quadrant> -> CInt -> IO CInt
#ccall p4est_comm_tree_info , Ptr <struct p4est> -> CInt -> Ptr CInt -> Ptr CInt -> Ptr (Ptr <struct p4est_quadrant>) -> Ptr (Ptr <struct p4est_quadrant>) -> IO ()
#ccall p4est_comm_neighborhood_owned , Ptr <struct p4est> -> CInt -> Ptr CInt -> Ptr CInt -> Ptr <struct p4est_quadrant> -> IO CInt
#ccall p4est_comm_sync_flag , Ptr <struct p4est> -> CInt -> <MPI_Op> -> IO CInt
#ccall p4est_comm_checksum , Ptr <struct p4est> -> CUInt -> CSize -> IO CUInt


-- * From future versions of 'p4est'
------------------------------------------------------------------------------
#ccall p4est_comm_is_contained , Ptr <struct p4est> -> CInt -> Ptr <struct p4est_quadrant> -> CInt -> IO CInt
