{-# OPTIONS_GHC -fno-warn-unused-imports #-}
#include <bindings.dsl.h>
#include "p4est_iterate.h"
module Bindings.P4est.P4estIterate where
import Foreign.Ptr
#strict_import


import Bindings.SC.ScContainers
import Bindings.P4est.P4est
import Bindings.P4est.P4estGhost


{- #ccall sc_extern_c_hack_3 , IO () -}

{- typedef struct p4est_iter_volume_info {
            p4est_t * p4est;
            p4est_ghost_t * ghost_layer;
            p4est_quadrant_t * quad;
            p4est_locidx_t quadid;
            p4est_topidx_t treeid;
        } p4est_iter_volume_info_t; -}
#starttype struct p4est_iter_volume_info
#field p4est , Ptr <struct p4est>
#field ghost_layer , Ptr <p4est_ghost_t>
#field quad , Ptr <struct p4est_quadrant>
#field quadid , CInt
#field treeid , CInt
#stoptype
#synonym_t p4est_iter_volume_info_t , <struct p4est_iter_volume_info>
#callback p4est_iter_volume_t , Ptr <struct p4est_iter_volume_info> -> Ptr () -> IO ()

{- typedef struct p4est_iter_face_side {
            p4est_topidx_t treeid;
            int8_t face;
            int8_t is_hanging;
            union p4est_iter_face_side_data {
                struct {
                    int8_t is_ghost; p4est_quadrant_t * quad; p4est_locidx_t quadid;
                } full;
                struct {
                    int8_t is_ghost[2];
                    p4est_quadrant_t * quad[2];
                    p4est_locidx_t quadid[2];
                } hanging;
            } is;
        } p4est_iter_face_side_t; -}
#starttype struct p4est_iter_face_side
#field treeid , CInt
#field face , CSChar
#field is_hanging , CSChar
-- --->
-- #field is , <union p4est_iter_face_side_data>
-- <---
#stoptype

#synonym_t p4est_iter_face_side_t , <struct p4est_iter_face_side>
{- typedef struct p4est_iter_face_info {
            p4est_t * p4est;
            p4est_ghost_t * ghost_layer;
            int8_t orientation;
            int8_t tree_boundary;
            sc_array_t sides;
        } p4est_iter_face_info_t; -}
#starttype struct p4est_iter_face_info
#field p4est , Ptr <struct p4est>
#field ghost_layer , Ptr <p4est_ghost_t>
#field orientation , CSChar
#field tree_boundary , CSChar
#field sides , <struct sc_array>
#stoptype
#synonym_t p4est_iter_face_info_t , <struct p4est_iter_face_info>
#callback p4est_iter_face_t , Ptr <struct p4est_iter_face_info> -> Ptr () -> IO ()
{- typedef struct p4est_iter_corner_side {
            p4est_topidx_t treeid;
            int8_t corner;
            int8_t is_ghost;
            p4est_quadrant_t * quad;
            p4est_locidx_t quadid;
            int8_t faces[2];
        } p4est_iter_corner_side_t; -}
#starttype struct p4est_iter_corner_side
#field treeid , CInt
#field corner , CSChar
#field is_ghost , CSChar
#field quad , Ptr <struct p4est_quadrant>
#field quadid , CInt
#array_field faces , CSChar
#stoptype
#synonym_t p4est_iter_corner_side_t , <struct p4est_iter_corner_side>
{- typedef struct p4est_iter_corner_info {
            p4est_t * p4est;
            p4est_ghost_t * ghost_layer;
            int8_t tree_boundary;
            sc_array_t sides;
        } p4est_iter_corner_info_t; -}
#starttype struct p4est_iter_corner_info
#field p4est , Ptr <struct p4est>
#field ghost_layer , Ptr <p4est_ghost_t>
#field tree_boundary , CSChar
#field sides , <struct sc_array>
#stoptype
#synonym_t p4est_iter_corner_info_t , <struct p4est_iter_corner_info>
#callback p4est_iter_corner_t , Ptr <struct p4est_iter_corner_info> -> Ptr () -> IO ()
#ccall p4est_iterate , Ptr <struct p4est> -> Ptr <p4est_ghost_t> -> Ptr () -> <p4est_iter_volume_t> -> <p4est_iter_face_t> -> <p4est_iter_corner_t> -> IO ()
#cinline p4est_iter_cside_array_index_int , Ptr <struct sc_array> -> CInt -> IO (Ptr <struct p4est_iter_corner_side>)
#cinline p4est_iter_cside_array_index , Ptr <struct sc_array> -> CSize -> IO (Ptr <struct p4est_iter_corner_side>)
#cinline p4est_iter_fside_array_index_int , Ptr <struct sc_array> -> CInt -> IO (Ptr <struct p4est_iter_face_side>)
#cinline p4est_iter_fside_array_index , Ptr <struct sc_array> -> CSize -> IO (Ptr <struct p4est_iter_face_side>)

{- #ccall sc_extern_c_hack_4 , IO () -}
