{-# OPTIONS_GHC -fno-warn-unused-imports #-}
#include <bindings.dsl.h>
-- #include "/usr/local/include/p4est_wrap.h"
#include "p4est_wrap.h"
module Bindings.P4est.P4estWrap where

import Foreign.Ptr
#strict_import


-- * Constants
------------------------------------------------------------------------------
#num P4EST_WRAP_NONE
#num P4EST_WRAP_REFINE
#num P4EST_WRAP_COARSEN


-- * Types
------------------------------------------------------------------------------
-- | Wrapped, mode flags.
{- typedef enum p4est_wrap_flags {
            P4EST_WRAP_NONE = 0,
            P4EST_WRAP_REFINE = 0x1,
            P4EST_WRAP_COARSEN = 0x2
        } p4est_wrap_flags_t; -}
#integral_t enum p4est_wrap_flags
#synonym_t p4est_wrap_flags_t , <enum p4est_wrap_flags>

------------------------------------------------------------------------------
-- | Wrapper type for 'p4est' parallel forests.
{- typedef struct p4est_wrap {
            void * user_pointer;
            int hollow;
            int coarsen_delay;
            int coarsen_affect;
            sc_refcount_t conn_rc;
            p4est_connectivity_t * conn;
            struct p4est_wrap * conn_owner;
            int p4est_dim;
            int p4est_half;
            int p4est_faces;
            int p4est_children;
            p4est_connect_type_t btype;
            p4est_replace_t replace_fn;
            p4est_t * p4est;
            int weight_exponent;
            uint8_t * flags, * temp_flags;
            p4est_locidx_t num_refine_flags, inside_counter, num_replaced;
            p4est_ghost_t * ghost;
            p4est_mesh_t * mesh;
            p4est_ghost_t * ghost_aux;
            p4est_mesh_t * mesh_aux;
            int match_aux;
        } p4est_wrap_t; -}
#starttype struct p4est_wrap
#field user_pointer , Ptr ()
#field hollow , CInt
#field coarsen_delay , CInt
#field coarsen_affect , CInt
#field conn_rc , <struct sc_refcount>
#field conn , Ptr <struct p4est_connectivity>
#field conn_owner , Ptr <struct p4est_wrap>
#field p4est_dim , CInt
#field p4est_half , CInt
#field p4est_faces , CInt
#field p4est_children , CInt
#field btype , <p4est_connect_type_t>
#field replace_fn , <p4est_replace_t>
#field p4est , Ptr <struct p4est>
#field weight_exponent , CInt
#field flags , Ptr CUChar
#field temp_flags , Ptr CUChar
#field num_refine_flags , CInt
#field inside_counter , CInt
#field num_replaced , CInt
#field ghost , Ptr <p4est_ghost_t>
#field mesh , Ptr <p4est_mesh_t>
#field ghost_aux , Ptr <p4est_ghost_t>
#field mesh_aux , Ptr <p4est_mesh_t>
#field match_aux , CInt
#stoptype
#synonym_t p4est_wrap_t , <struct p4est_wrap>

------------------------------------------------------------------------------
-- | Leaf-quadrant iterator data-type.
{- typedef struct p4est_wrap_leaf {
            p4est_wrap_t * pp;
            p4est_topidx_t which_tree;
            p4est_locidx_t which_quad;
            p4est_locidx_t local_quad;
            p4est_tree_t * tree;
            sc_array_t * tquadrants;
            p4est_quadrant_t * quad;
            int is_mirror;
            sc_array_t * mirrors;
            p4est_locidx_t nm;
            p4est_locidx_t next_mirror_quadrant;
        } p4est_wrap_leaf_t; -}
#starttype struct p4est_wrap_leaf
#field pp , Ptr <struct p4est_wrap>
#field which_tree , CInt
#field which_quad , CInt
#field local_quad , CInt
#field tree , Ptr <struct p4est_tree>
#field tquadrants , Ptr <struct sc_array>
#field quad , Ptr <struct p4est_quadrant>
#field is_mirror , CInt
#field mirrors , Ptr <struct sc_array>
#field nm , CInt
#field next_mirror_quadrant , CInt
#stoptype
#synonym_t p4est_wrap_leaf_t , <struct p4est_wrap_leaf>


-- * Functions
------------------------------------------------------------------------------
#ccall p4est_wrap_new_world , CInt -> IO (Ptr <struct p4est_wrap>)

-- ** Constructors & destructors
------------------------------------------------------------------------------
#ccall p4est_wrap_new_conn , <MPI_Comm> -> Ptr <struct p4est_connectivity> -> CInt -> IO (Ptr <struct p4est_wrap>)
#ccall p4est_wrap_new_p4est , Ptr <struct p4est> -> CInt -> <p4est_connect_type_t> -> <p4est_replace_t> -> Ptr () -> IO (Ptr <struct p4est_wrap>)
#ccall p4est_wrap_new_ext , <MPI_Comm> -> Ptr <struct p4est_connectivity> -> CInt -> CInt -> <p4est_connect_type_t> -> <p4est_replace_t> -> Ptr () -> IO (Ptr <struct p4est_wrap>)
#ccall p4est_wrap_new_copy , Ptr <struct p4est_wrap> -> CSize -> <p4est_replace_t> -> Ptr () -> IO (Ptr <struct p4est_wrap>)
#ccall p4est_wrap_new_unitsquare , <MPI_Comm> -> CInt -> IO (Ptr <struct p4est_wrap>)
#ccall p4est_wrap_new_periodic , <MPI_Comm> -> CInt -> IO (Ptr <struct p4est_wrap>)
#ccall p4est_wrap_new_rotwrap , <MPI_Comm> -> CInt -> IO (Ptr <struct p4est_wrap>)
#ccall p4est_wrap_new_corner , <MPI_Comm> -> CInt -> IO (Ptr <struct p4est_wrap>)
#ccall p4est_wrap_new_pillow , <MPI_Comm> -> CInt -> IO (Ptr <struct p4est_wrap>)
#ccall p4est_wrap_new_moebius , <MPI_Comm> -> CInt -> IO (Ptr <struct p4est_wrap>)
#ccall p4est_wrap_new_cubed , <MPI_Comm> -> CInt -> IO (Ptr <struct p4est_wrap>)
#ccall p4est_wrap_new_disk , <MPI_Comm> -> CInt -> CInt -> CInt -> IO (Ptr <struct p4est_wrap>)
#ccall p4est_wrap_new_brick , <MPI_Comm> -> CInt -> CInt -> CInt -> CInt -> CInt -> IO (Ptr <struct p4est_wrap>)

#ccall p4est_wrap_destroy , Ptr <struct p4est_wrap> -> IO ()

-- ** Data accessors
------------------------------------------------------------------------------
#ccall p4est_wrap_get_ghost , Ptr <struct p4est_wrap> -> IO (Ptr <p4est_ghost_t>)
#ccall p4est_wrap_get_mesh , Ptr <struct p4est_wrap> -> IO (Ptr <p4est_mesh_t>)

-- ** Wrapper flag-modification functions
------------------------------------------------------------------------------
#ccall p4est_wrap_set_hollow , Ptr <struct p4est_wrap> -> CInt -> IO ()
#ccall p4est_wrap_set_coarsen_delay , Ptr <struct p4est_wrap> -> CInt -> CInt -> IO ()
#ccall p4est_wrap_mark_refine , Ptr <struct p4est_wrap> -> CInt -> CInt -> IO ()
#ccall p4est_wrap_mark_coarsen , Ptr <struct p4est_wrap> -> CInt -> CInt -> IO ()

-- ** Forest modification functions
------------------------------------------------------------------------------
#ccall p4est_wrap_adapt , Ptr <struct p4est_wrap> -> IO CInt
#ccall p4est_wrap_partition , Ptr <struct p4est_wrap> -> CInt -> Ptr CInt -> Ptr CInt -> Ptr CInt -> IO CInt
#ccall p4est_wrap_complete , Ptr <struct p4est_wrap> -> IO ()

-- ** Iterator functions
------------------------------------------------------------------------------
#ccall p4est_wrap_leaf_first , Ptr <struct p4est_wrap> -> CInt -> IO (Ptr <struct p4est_wrap_leaf>)
#ccall p4est_wrap_leaf_next , Ptr <struct p4est_wrap_leaf> -> IO (Ptr <struct p4est_wrap_leaf>)
