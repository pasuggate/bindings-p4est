{-# OPTIONS_GHC -fno-warn-unused-imports #-}
#include <bindings.dsl.h>
#include "p4est_lnodes.h"
module Bindings.P4est.P4estLnodes where
import Foreign.Ptr
#strict_import

import Bindings.SC.Sc
import Bindings.SC.ScContainers

import Bindings.P4est.P4est
import Bindings.P4est.P4estGhost

{- #ccall sc_extern_c_hack_3 , IO () -}

{- typedef int8_t p4est_lnodes_code_t; -}
#synonym_t p4est_lnodes_code_t , CSChar
{- typedef struct p4est_lnodes {
            MPI_Comm mpicomm;
            p4est_locidx_t num_local_nodes;
            p4est_locidx_t owned_count;
            p4est_gloidx_t global_offset;
            p4est_gloidx_t * nonlocal_nodes;
            sc_array_t * sharers;
            p4est_locidx_t * global_owned_count;
            int degree, vnodes;
            p4est_locidx_t num_local_elements;
            p4est_lnodes_code_t * face_code;
            p4est_locidx_t * element_nodes;
        } p4est_lnodes_t; -}
#starttype struct p4est_lnodes
#field mpicomm , <MPI_Comm>
#field num_local_nodes , CInt
#field owned_count , CInt
#field global_offset , CLong
#field nonlocal_nodes , Ptr CLong
#field sharers , Ptr <struct sc_array>
#field global_owned_count , Ptr CInt
#field degree , CInt
#field vnodes , CInt
#field num_local_elements , CInt
#field face_code , Ptr CSChar
#field element_nodes , Ptr CInt
#stoptype
#synonym_t p4est_lnodes_t , <struct p4est_lnodes>
{- typedef struct p4est_lnodes_rank {
            int rank;
            sc_array_t shared_nodes;
            p4est_locidx_t shared_mine_offset, shared_mine_count;
            p4est_locidx_t owned_offset, owned_count;
        } p4est_lnodes_rank_t; -}
#starttype struct p4est_lnodes_rank
#field rank , CInt
#field shared_nodes , <struct sc_array>
#field shared_mine_offset , CInt
#field shared_mine_count , CInt
#field owned_offset , CInt
#field owned_count , CInt
#stoptype
#synonym_t p4est_lnodes_rank_t , <struct p4est_lnodes_rank>
#cinline p4est_lnodes_decode , CSChar -> Ptr CInt -> IO CInt
#ccall p4est_lnodes_new , Ptr <struct p4est> -> Ptr <p4est_ghost_t> -> CInt -> IO (Ptr <struct p4est_lnodes>)
#ccall p4est_lnodes_destroy , Ptr <struct p4est_lnodes> -> IO ()
#ccall p4est_ghost_support_lnodes , Ptr <struct p4est> -> Ptr <struct p4est_lnodes> -> Ptr <p4est_ghost_t> -> IO ()
#ccall p4est_ghost_expand_by_lnodes , Ptr <struct p4est> -> Ptr <struct p4est_lnodes> -> Ptr <p4est_ghost_t> -> IO ()
{- typedef struct p4est_lnodes_buffer {
            sc_array_t * requests;
            sc_array_t * send_buffers;
            sc_array_t * recv_buffers;
        } p4est_lnodes_buffer_t; -}
#starttype struct p4est_lnodes_buffer
#field requests , Ptr <struct sc_array>
#field send_buffers , Ptr <struct sc_array>
#field recv_buffers , Ptr <struct sc_array>
#stoptype
#synonym_t p4est_lnodes_buffer_t , <struct p4est_lnodes_buffer>
#ccall p4est_lnodes_share_owned_begin , Ptr <struct sc_array> -> Ptr <struct p4est_lnodes> -> IO (Ptr <struct p4est_lnodes_buffer>)
#ccall p4est_lnodes_share_owned_end , Ptr <struct p4est_lnodes_buffer> -> IO ()
#ccall p4est_lnodes_share_owned , Ptr <struct sc_array> -> Ptr <struct p4est_lnodes> -> IO ()
#ccall p4est_lnodes_share_all_begin , Ptr <struct sc_array> -> Ptr <struct p4est_lnodes> -> IO (Ptr <struct p4est_lnodes_buffer>)
#ccall p4est_lnodes_share_all_end , Ptr <struct p4est_lnodes_buffer> -> IO ()
#ccall p4est_lnodes_share_all , Ptr <struct sc_array> -> Ptr <struct p4est_lnodes> -> IO (Ptr <struct p4est_lnodes_buffer>)
#ccall p4est_lnodes_buffer_destroy , Ptr <struct p4est_lnodes_buffer> -> IO ()
#cinline p4est_lnodes_rank_array_index_int , Ptr <struct sc_array> -> CInt -> IO (Ptr <struct p4est_lnodes_rank>)
#cinline p4est_lnodes_rank_array_index , Ptr <struct sc_array> -> CSize -> IO (Ptr <struct p4est_lnodes_rank>)
#cinline p4est_lnodes_global_index , Ptr <struct p4est_lnodes> -> CInt -> IO CLong

{- #ccall sc_extern_c_hack_4 , IO () -}
