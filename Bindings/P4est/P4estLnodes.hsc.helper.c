#include <bindings.cmacros.h>
#include <sc.h>
#include <p4est.h>
#include <p4est_lnodes.h>

BC_INLINE2(p4est_lnodes_decode, p4est_lnodes_code_t, int*, int)
BC_INLINE2(p4est_lnodes_rank_array_index_int, sc_array_t*, int, p4est_lnodes_rank_t*)
BC_INLINE2(p4est_lnodes_rank_array_index, sc_array_t*, size_t, p4est_lnodes_rank_t*)
BC_INLINE2(p4est_lnodes_global_index, p4est_lnodes_t*, p4est_locidx_t, p4est_gloidx_t)
