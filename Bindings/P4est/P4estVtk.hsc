{-# OPTIONS_GHC -fno-warn-unused-imports #-}
#include <bindings.dsl.h>
#include "p4est_vtk.h"
module Bindings.P4est.P4estVtk where
import Foreign.Ptr
#strict_import

import Bindings.SC.ScContainers
import Bindings.P4est.P4est
import Bindings.P4est.P4estGeometry


------------------------------------------------------------------------------
#if 1 /* new_P4 */
------------------------------------------------------------------------------
{- typedef struct p4est_vtk_context p4est_vtk_context_t; -}
#opaque_t struct p4est_vtk_context
#synonym_t p4est_vtk_context_t , <struct p4est_vtk_context>
#ccall p4est_vtk_write_file , Ptr <struct p4est> -> Ptr <struct p4est_geometry> -> CString -> IO ()
#ccall p4est_vtk_context_new , Ptr <struct p4est> -> CString -> IO (Ptr <struct p4est_vtk_context>)
#ccall p4est_vtk_context_set_geom , Ptr <struct p4est_vtk_context> -> Ptr <struct p4est_geometry> -> IO ()
#ccall p4est_vtk_context_set_scale , Ptr <struct p4est_vtk_context> -> CDouble -> IO ()
#ccall p4est_vtk_context_set_continuous , Ptr <struct p4est_vtk_context> -> CInt -> IO ()
#ccall p4est_vtk_context_destroy , Ptr <struct p4est_vtk_context> -> IO ()
#ccall p4est_vtk_write_header , Ptr <struct p4est_vtk_context> -> IO (Ptr <struct p4est_vtk_context>)
-- #ccall p4est_vtk_write_cell_dataf , Ptr <struct p4est_vtk_context> -> CInt -> CInt -> CInt -> CInt -> CInt -> CInt -> IO (Ptr <struct p4est_vtk_context>)
-- #ccall p4est_vtk_write_cell_data , Ptr <struct p4est_vtk_context> -> CInt -> CInt -> CInt -> CInt -> CInt -> CInt -> Ptr CString -> Ptr (Ptr <struct sc_array>) -> IO (Ptr <struct p4est_vtk_context>)
-- #ccall p4est_vtk_write_point_dataf , Ptr <struct p4est_vtk_context> -> CInt -> CInt -> IO (Ptr <struct p4est_vtk_context>)
#ccall p4est_vtk_write_footer , Ptr <struct p4est_vtk_context> -> IO CInt

------------------------------------------------------------------------------
#else /* !new_P4 */
------------------------------------------------------------------------------

#ccall p4est_vtk_write_file , Ptr <struct p4est> -> Ptr <struct p4est_geometry> -> CString -> IO ()
#ccall p4est_vtk_write_all , Ptr <struct p4est> -> Ptr <struct p4est_geometry> -> CDouble -> CInt -> CInt -> CInt -> CInt -> CInt -> CInt -> CString -> IO ()
#ccall p4est_vtk_write_header , Ptr <struct p4est> -> Ptr <struct p4est_geometry> -> CDouble -> CInt -> CInt -> CInt -> CInt -> CString -> CString -> CString -> IO CInt
#ccall p4est_vtk_write_point_scalar , Ptr <struct p4est> -> Ptr <struct p4est_geometry> -> CString -> CString -> Ptr CDouble -> IO CInt
#ccall p4est_vtk_write_point_vector , Ptr <struct p4est> -> Ptr <struct p4est_geometry> -> CString -> CString -> Ptr CDouble -> IO CInt
#ccall p4est_vtk_write_footer , Ptr <struct p4est> -> CString -> IO CInt

------------------------------------------------------------------------------
#endif /* new_P4 */
------------------------------------------------------------------------------
