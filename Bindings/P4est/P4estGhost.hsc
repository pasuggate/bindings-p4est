{-# OPTIONS_GHC -fno-warn-unused-imports #-}
#include <bindings.dsl.h>
#include "p4est_ghost.h"
module Bindings.P4est.P4estGhost where
import Foreign.Ptr
#strict_import


import Bindings.SC.ScContainers
import Bindings.P4est.P4est
import Bindings.P4est.P4estConnectivity


{- typedef struct {
            int mpisize;
            p4est_topidx_t num_trees;
            p4est_connect_type_t btype;
            sc_array_t ghosts;
            p4est_locidx_t * tree_offsets;
            p4est_locidx_t * proc_offsets;
            sc_array_t mirrors;
            p4est_locidx_t * mirror_tree_offsets;
            p4est_locidx_t * mirror_proc_mirrors;
            p4est_locidx_t * mirror_proc_offsets;
            p4est_locidx_t * mirror_proc_fronts;
            p4est_locidx_t * mirror_proc_front_offsets;
        } p4est_ghost_t; -}
#starttype p4est_ghost_t
#field mpisize , CInt
#field num_trees , CInt
#field btype , <p4est_connect_type_t>
#field ghosts , <struct sc_array>
#field tree_offsets , Ptr CInt
#field proc_offsets , Ptr CInt
#field mirrors , <struct sc_array>
#field mirror_tree_offsets , Ptr CInt
#field mirror_proc_mirrors , Ptr CInt
#field mirror_proc_offsets , Ptr CInt
#field mirror_proc_fronts , Ptr CInt
#field mirror_proc_front_offsets , Ptr CInt
#stoptype

#ccall p4est_ghost_is_valid , Ptr <struct p4est> -> Ptr <p4est_ghost_t> -> IO CInt
#ccall p4est_ghost_memory_used , Ptr <p4est_ghost_t> -> IO CSize
#ccall p4est_quadrant_find_owner , Ptr <struct p4est> -> CInt -> CInt -> Ptr <struct p4est_quadrant> -> IO CInt
#ccall p4est_ghost_new , Ptr <struct p4est> -> <p4est_connect_type_t> -> IO (Ptr <p4est_ghost_t>)
#ccall p4est_ghost_destroy , Ptr <p4est_ghost_t> -> IO ()
#ccall p4est_ghost_bsearch , Ptr <p4est_ghost_t> -> CInt -> CInt -> Ptr <struct p4est_quadrant> -> IO CLong
#ccall p4est_ghost_contains , Ptr <p4est_ghost_t> -> CInt -> CInt -> Ptr <struct p4est_quadrant> -> IO CLong
#ccall p4est_face_quadrant_exists , Ptr <struct p4est> -> Ptr <p4est_ghost_t> -> CInt -> Ptr <struct p4est_quadrant> -> Ptr CInt -> Ptr CInt -> Ptr CInt -> IO CInt
#ccall p4est_quadrant_exists , Ptr <struct p4est> -> Ptr <p4est_ghost_t> -> CInt -> Ptr <struct p4est_quadrant> -> Ptr <struct sc_array> -> Ptr <struct sc_array> -> Ptr <struct sc_array> -> IO CInt
#ccall p4est_is_balanced , Ptr <struct p4est> -> <p4est_connect_type_t> -> IO CInt
#ccall p4est_ghost_checksum , Ptr <struct p4est> -> Ptr <p4est_ghost_t> -> IO CUInt
#ccall p4est_ghost_exchange_data , Ptr <struct p4est> -> Ptr <p4est_ghost_t> -> Ptr () -> IO ()
#ccall p4est_ghost_exchange_custom , Ptr <struct p4est> -> Ptr <p4est_ghost_t> -> CSize -> Ptr (Ptr ()) -> Ptr () -> IO ()
#ccall p4est_ghost_exchange_custom_levels , Ptr <struct p4est> -> Ptr <p4est_ghost_t> -> CInt -> CInt -> CSize -> Ptr (Ptr ()) -> Ptr () -> IO ()
#ccall p4est_ghost_expand , Ptr <struct p4est> -> Ptr <p4est_ghost_t> -> IO ()
