#include <bindings.cmacros.h>
#include <sc.h>
#include <p4est.h>

#define __USE_FUTURE__
#ifdef  __USE_FUTURE__
#include <p4est_bits.h>
#include <p4est_communication.h>

/**
 * NOTE: comes from a later version of 'p4est'.
 */

/** Test whether a quadrant is fully contained in a rank's owned regien.
 * This function may return false when \ref p4est_comm_is_owner returns true.
 * \param [in] rank    Rank whose ownership is tested.
 *                     Assumes a forest with no overlaps.
 * \return true if rank is the owner of the whole area of the quadrant.
 */
int
p4est_comm_is_contained (p4est_t * p4est, p4est_locidx_t which_tree,
                         const p4est_quadrant_t * q, int rank)
{
  p4est_topidx_t      ctree;
  p4est_quadrant_t    qlast;
  const p4est_quadrant_t *cur;

  P4EST_ASSERT (p4est != NULL && p4est->connectivity != NULL);
  P4EST_ASSERT (p4est->global_first_position != NULL);
  P4EST_ASSERT (0 <= which_tree &&
                which_tree < p4est->connectivity->num_trees);
  P4EST_ASSERT (q != NULL);
  P4EST_ASSERT (0 <= rank && rank < p4est->mpisize);
  P4EST_ASSERT (p4est_quadrant_is_node (q, 1) || p4est_quadrant_is_valid (q));

  /* check whether q begins on a lower processor than rank */
  cur = &p4est->global_first_position[rank];
  P4EST_ASSERT (cur->level == P4EST_QMAXLEVEL);
  ctree = cur->p.which_tree;
  if (which_tree < ctree ||
      (which_tree == ctree &&
       (p4est_quadrant_compare (q, cur) < 0 &&
        (q->x != cur->x || q->y != cur->y
#ifdef P4_TO_P8
         || q->z != cur->z
#endif
        )))) {
    return 0;
  }

  /* check whether q ends on a higher processor than rank */
  ++cur;
  P4EST_ASSERT (cur == &p4est->global_first_position[rank + 1]);
  P4EST_ASSERT (cur->level == P4EST_QMAXLEVEL);
  ctree = cur->p.which_tree;
  if (which_tree > ctree ||
      (which_tree == ctree &&
       (p4est_quadrant_last_descendant (q, &qlast, P4EST_QMAXLEVEL),
        p4est_quadrant_compare (cur, &qlast) <= 0))) {
    return 0;
  }

  /* the quadrant lies fully in the ownership region of rank */
  return 1;
}


/** Compute a specific child of a quadrant.
 * \param [in]     q    Input quadrant.
 * \param [in,out] r    Existing quadrant whose Morton index will be filled
 *                      with the coordinates of its child no. \b child_id.
 * \param [in] child_id The id of the child computed, 0..3.
 */
void
p4est_quadrant_child (const p4est_quadrant_t * q, p4est_quadrant_t * r,
                      int child_id)
{
  const p4est_qcoord_t shift = P4EST_QUADRANT_LEN (q->level + 1);

  P4EST_ASSERT (p4est_quadrant_is_extended (q));
  P4EST_ASSERT (q->level < P4EST_QMAXLEVEL);
  P4EST_ASSERT (child_id >= 0 && child_id < P4EST_CHILDREN);

  r->x = child_id & 0x01 ? (q->x | shift) : q->x;
  r->y = child_id & 0x02 ? (q->y | shift) : q->y;
#ifdef P4_TO_P8
  r->z = child_id & 0x04 ? (q->z | shift) : q->z;
#endif
  r->level = q->level + 1;
  P4EST_ASSERT (p4est_quadrant_is_parent (q, r));
}

#endif /* __USE_FUTURE__ */

BC_INLINE1(P4EST_LAST_OFFSET, int, int)
BC_INLINE1(P4EST_QUADRANT_LEN, int, int)

BC_INLINE2(p4est_tree_array_index, sc_array_t*, p4est_topidx_t, p4est_tree_t*)
BC_INLINE2(p4est_quadrant_array_index, sc_array_t*, size_t, p4est_quadrant_t*)
BC_INLINE1(p4est_quadrant_array_push, sc_array_t*, p4est_quadrant_t*)
BC_INLINE1(p4est_quadrant_mempool_alloc, sc_mempool_t*, p4est_quadrant_t*)
BC_INLINE1(p4est_quadrant_list_pop, sc_list_t*, p4est_quadrant_t*)
