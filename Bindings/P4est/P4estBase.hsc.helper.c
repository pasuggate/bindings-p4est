#include <bindings.cmacros.h>
#include <stdint.h>
#include <p4est.h>

BC_INLINE0VOID(p4est_log_indent_push)
BC_INLINE0VOID(p4est_log_indent_pop)
BC_INLINE1(p4est_topidx_hash2, const p4est_topidx_t*, unsigned)
BC_INLINE1(p4est_topidx_hash3, const p4est_topidx_t*, unsigned)
BC_INLINE1(p4est_topidx_hash4, const p4est_topidx_t*, unsigned)
BC_INLINE2(p4est_topidx_is_sorted, p4est_topidx_t*, int, int)
BC_INLINE2VOID(p4est_topidx_bsort, p4est_topidx_t*, int)
BC_INLINE3(p4est_partition_cut_uint64, uint64_t, int, int, uint64_t)
BC_INLINE3(p4est_partition_cut_gloidx, p4est_gloidx_t, int, int, p4est_gloidx_t)
