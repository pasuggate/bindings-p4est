{-# OPTIONS_GHC -fno-warn-unused-imports #-}
#include <bindings.dsl.h>
#include "p4est_geometry.h"
module Bindings.P4est.P4estGeometry where
import Foreign.Ptr
#strict_import

import Bindings.P4est.P4estConnectivity


{- typedef struct p4est_geometry p4est_geometry_t; -}
#synonym_t p4est_geometry_t , <struct p4est_geometry>

#callback p4est_geometry_X_t , Ptr <struct p4est_geometry> -> CInt -> Ptr CDouble -> Ptr CDouble -> IO ()
#callback p4est_geometry_destroy_t , Ptr <struct p4est_geometry> -> IO ()

{- struct p4est_geometry {
    const char * name;
    void * user;
    p4est_geometry_X_t X;
    p4est_geometry_destroy_t destroy;
}; -}
#starttype struct p4est_geometry
#field name , CString
#field user , Ptr ()
#field X , <p4est_geometry_X_t>
#field destroy , <p4est_geometry_destroy_t>
#stoptype

#ccall p4est_geometry_destroy , Ptr <struct p4est_geometry> -> IO ()
#ccall p4est_geometry_new_connectivity , Ptr <struct p4est_connectivity> -> IO (Ptr <struct p4est_geometry>)
