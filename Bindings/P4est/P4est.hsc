{-# OPTIONS_GHC -fno-warn-unused-imports #-}
#include <bindings.dsl.h>
-- #include "p4est.h"
#include "p4est_extended.h"
module Bindings.P4est.P4est where

#strict_import
import Foreign.Ptr
import Bindings.SC.Sc
import Bindings.SC.ScContainers

import Bindings.P4est.P4estConnectivity


-- * Constants
------------------------------------------------------------------------------
#num P4EST_MAXLEVEL
#num P4EST_QMAXLEVEL
#num P4EST_ROOT_LEN


------------------------------------------------------------------------------
-- | From `p4est_extended`, to break cyclic dependencies.
{- struct p4est_inspect {
    int use_balance_ranges;
    int use_balance_ranges_notify;
    int use_balance_verify;
    int balance_max_ranges;
    size_t balance_A_count_in;
    size_t balance_A_count_out;
    size_t balance_comm_sent;
    size_t balance_comm_nzpeers;
    size_t balance_B_count_in;
    size_t balance_B_count_out;
    size_t balance_zero_sends[2], balance_zero_receives[2];
    double balance_A;
    double balance_comm;
    double balance_B;
    double balance_ranges;
    double balance_notify;
    double balance_notify_allgather;
    int use_B;
}; -}
#starttype struct p4est_inspect
#field use_balance_ranges , CInt
#field use_balance_ranges_notify , CInt
#field use_balance_verify , CInt
#field balance_max_ranges , CInt
#field balance_A_count_in , CSize
#field balance_A_count_out , CSize
#field balance_comm_sent , CSize
#field balance_comm_nzpeers , CSize
#field balance_B_count_in , CSize
#field balance_B_count_out , CSize
#array_field balance_zero_sends , CSize
#array_field balance_zero_receives , CSize
#field balance_A , CDouble
#field balance_comm , CDouble
#field balance_B , CDouble
#field balance_ranges , CDouble
#field balance_notify , CDouble
#field balance_notify_allgather , CDouble
#field use_B , CInt
#stoptype

{- typedef struct p4est_quadrant {
            p4est_qcoord_t x, y;
            int8_t level, pad8;
            int16_t pad16;
            union p4est_quadrant_data {
                void * user_data;
                long user_long;
                int user_int;
                p4est_topidx_t which_tree;
                struct {
                    p4est_topidx_t which_tree; int owner_rank;
                } piggy1;
                struct {
                    p4est_topidx_t which_tree; p4est_topidx_t from_tree;
                } piggy2;
                struct {
                    p4est_topidx_t which_tree; p4est_locidx_t local_num;
                } piggy3;
            } p;
        } p4est_quadrant_t; -}
#starttype struct p4est_quadrant
#field x , CInt
#field y , CInt
#field level , CSChar
#field pad8 , CSChar
#field pad16 , CShort
-- --->
#field p.user_data , Ptr ()
#field p.piggy1.which_tree , CInt
#field p.piggy1.owner_rank , CInt
#field p.piggy3.which_tree , CInt
#field p.piggy3.local_num  , CInt
-- #field p , <union p4est_quadrant_data>
-- <---
#stoptype
#synonym_t p4est_quadrant_t , <struct p4est_quadrant>

{- typedef struct p4est_tree {
            sc_array_t quadrants;
            p4est_quadrant_t first_desc, last_desc;
            p4est_locidx_t quadrants_offset;
            p4est_locidx_t quadrants_per_level[30 + 1];
            int8_t maxlevel;
        } p4est_tree_t; -}
#starttype struct p4est_tree
#field quadrants , <struct sc_array>
#field first_desc , <struct p4est_quadrant>
#field last_desc , <struct p4est_quadrant>
#field quadrants_offset , CInt
#array_field quadrants_per_level , CInt
#field maxlevel , CSChar
#stoptype
#synonym_t p4est_tree_t , <struct p4est_tree>

{- typedef struct p4est_inspect p4est_inspect_t; -}
-- #opaque_t struct p4est_inspect
#synonym_t p4est_inspect_t , <struct p4est_inspect>

{- typedef struct p4est {
            MPI_Comm mpicomm;
            int mpisize, mpirank;
            size_t data_size;
            void * user_pointer;
            p4est_topidx_t first_local_tree;
            p4est_topidx_t last_local_tree;
            p4est_locidx_t local_num_quadrants;
            p4est_gloidx_t global_num_quadrants;
            p4est_gloidx_t * global_first_quadrant;
            p4est_quadrant_t * global_first_position;
            p4est_connectivity_t * connectivity;
            sc_array_t * trees;
            sc_mempool_t * user_data_pool;
            sc_mempool_t * quadrant_pool;
            p4est_inspect_t * inspect;
        } p4est_t; -}
#starttype struct p4est
#field mpicomm , <MPI_Comm>
#field mpisize , CInt
#field mpirank , CInt
#field data_size , CSize
#field user_pointer , Ptr ()
#field first_local_tree , CInt
#field last_local_tree , CInt
#field local_num_quadrants , CInt
#field global_num_quadrants , CLong
#field global_first_quadrant , Ptr CLong
#field global_first_position , Ptr <struct p4est_quadrant>
#field connectivity , Ptr <struct p4est_connectivity>
#field trees , Ptr <struct sc_array>
#field user_data_pool , Ptr <struct sc_mempool>
#field quadrant_pool , Ptr <struct sc_mempool>
#field inspect , Ptr <struct p4est_inspect>
#stoptype
#synonym_t p4est_t , <struct p4est>

#ccall p4est_memory_used , Ptr <struct p4est> -> IO CSize

#callback p4est_init_t , Ptr <struct p4est> -> CInt -> Ptr <struct p4est_quadrant> -> IO ()
#callback p4est_refine_t , Ptr <struct p4est> -> CInt -> Ptr <struct p4est_quadrant> -> IO CInt
#callback p4est_coarsen_t , Ptr <struct p4est> -> CInt -> Ptr (Ptr <struct p4est_quadrant>) -> IO CInt
#callback p4est_weight_t , Ptr <struct p4est> -> CInt -> Ptr <struct p4est_quadrant> -> IO CInt

#globalvar P4EST_DATA_UNINITIALIZED , Ptr ()
#ccall p4est_qcoord_to_vertex , Ptr <struct p4est_connectivity> -> CInt -> CInt -> CInt -> Ptr CDouble -> IO ()
#ccall p4est_new , <MPI_Comm> -> Ptr <struct p4est_connectivity> -> CSize -> <p4est_init_t> -> Ptr () -> IO (Ptr <struct p4est>)
#ccall p4est_destroy , Ptr <struct p4est> -> IO ()
#ccall p4est_copy , Ptr <struct p4est> -> CInt -> IO (Ptr <struct p4est>)
#ccall p4est_reset_data , Ptr <struct p4est> -> CSize -> <p4est_init_t> -> Ptr () -> IO ()
#ccall p4est_refine , Ptr <struct p4est> -> CInt -> <p4est_refine_t> -> <p4est_init_t> -> IO ()
#ccall p4est_coarsen , Ptr <struct p4est> -> CInt -> <p4est_coarsen_t> -> <p4est_init_t> -> IO ()
#ccall p4est_balance , Ptr <struct p4est> -> <p4est_connect_type_t> -> <p4est_init_t> -> IO ()
#ccall p4est_partition , Ptr <struct p4est> -> CInt -> <p4est_weight_t> -> IO ()
#ccall p4est_checksum , Ptr <struct p4est> -> IO CUInt
#ccall p4est_save , CString -> Ptr <struct p4est> -> CInt -> IO ()
#ccall p4est_load , CString -> <MPI_Comm> -> CSize -> CInt -> Ptr () -> Ptr (Ptr <struct p4est_connectivity>) -> IO (Ptr <struct p4est>)


-- * Inlines and macros
------------------------------------------------------------------------------
#cinline P4EST_LAST_OFFSET , CInt -> CInt
#cinline P4EST_QUADRANT_LEN , CInt -> CInt

#cinline p4est_tree_array_index , Ptr <struct sc_array> -> CInt -> IO (Ptr <struct p4est_tree>)
#cinline p4est_quadrant_array_index , Ptr <struct sc_array> -> CSize -> IO (Ptr <struct p4est_quadrant>)
#cinline p4est_quadrant_array_push , Ptr <struct sc_array> -> IO (Ptr <struct p4est_quadrant>)
#cinline p4est_quadrant_mempool_alloc , Ptr <struct sc_mempool> -> IO (Ptr <struct p4est_quadrant>)
#cinline p4est_quadrant_list_pop , Ptr <struct sc_list> -> IO (Ptr <struct p4est_quadrant>)
