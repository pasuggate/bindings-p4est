{-# OPTIONS_GHC -fno-warn-unused-imports #-}
#include <bindings.dsl.h>
#include "p4est_connectivity.h"
module Bindings.P4est.P4estConnectivity where
import Foreign.Ptr
#strict_import


import Bindings.SC.Sc
import Bindings.SC.ScContainers
import Bindings.SC.ScIo


{- typedef enum {
            P4EST_CONNECT_FACE = 21,
            P4EST_CONNECT_CORNER = 22,
            P4EST_CONNECT_FULL = P4EST_CONNECT_CORNER
        } p4est_connect_type_t; -}
#integral_t p4est_connect_type_t
#num P4EST_CONNECT_FACE
#num P4EST_CONNECT_CORNER
#num P4EST_CONNECT_FULL

{- typedef enum {
            P4EST_CONN_ENCODE_NONE = SC_IO_ENCODE_NONE, P4EST_CONN_ENCODE_LAST
        } p4est_connectivity_encode_t; -}
#integral_t p4est_connectivity_encode_t
#num P4EST_CONN_ENCODE_NONE
#num P4EST_CONN_ENCODE_LAST

#ccall p4est_connect_type_int , <p4est_connect_type_t> -> IO CInt
#ccall p4est_connect_type_string , <p4est_connect_type_t> -> IO CString

{- typedef struct p4est_connectivity {
            p4est_topidx_t num_vertices;
            p4est_topidx_t num_trees;
            p4est_topidx_t num_corners;
            double * vertices;
            p4est_topidx_t * tree_to_vertex;
            size_t tree_attr_bytes;
            char * tree_to_attr;
            p4est_topidx_t * tree_to_tree;
            int8_t * tree_to_face;
            p4est_topidx_t * tree_to_corner;
            p4est_topidx_t * ctt_offset;
            p4est_topidx_t * corner_to_tree;
            int8_t * corner_to_corner;
        } p4est_connectivity_t; -}
#starttype struct p4est_connectivity
#field num_vertices , CInt
#field num_trees , CInt
#field num_corners , CInt
#field vertices , Ptr CDouble
#field tree_to_vertex , Ptr CInt
#field tree_attr_bytes , CSize
#field tree_to_attr , CString
#field tree_to_tree , Ptr CInt
#field tree_to_face , Ptr CSChar
#field tree_to_corner , Ptr CInt
#field ctt_offset , Ptr CInt
#field corner_to_tree , Ptr CInt
#field corner_to_corner , Ptr CSChar
#stoptype
#synonym_t p4est_connectivity_t , <struct p4est_connectivity>

#ccall p4est_connectivity_memory_used , Ptr <struct p4est_connectivity> -> IO CSize

{- typedef struct {
            p4est_topidx_t ntree; int8_t ncorner;
        } p4est_corner_transform_t; -}
#starttype p4est_corner_transform_t
#field ntree , CInt
#field ncorner , CSChar
#stoptype

{- typedef struct {
            p4est_topidx_t icorner; sc_array_t corner_transforms;
        } p4est_corner_info_t; -}
#starttype p4est_corner_info_t
#field icorner , CInt
#field corner_transforms , <struct sc_array>
#stoptype

{-
#globalarray p4est_face_corners , CInt
#globalarray p4est_face_dual , CInt
#globalarray p4est_corner_faces , CInt
#globalarray p4est_corner_face_corners , CInt
#globalarray p4est_child_corner_faces , CInt
-}


------------------------------------------------------------------------------
#if 1 /* new_P4 */
------------------------------------------------------------------------------
#ccall p4est_connectivity_face_neighbor_face_corner , CInt -> CInt -> CInt -> CInt -> IO CInt
#ccall p4est_connectivity_face_neighbor_corner , CInt -> CInt -> CInt -> CInt -> IO CInt
------------------------------------------------------------------------------
#else /* !new_P4 */
------------------------------------------------------------------------------
#ccall p4est_connectivity_face_neighbor_corner_orientation , CInt -> CInt -> CInt -> CInt -> IO CInt
------------------------------------------------------------------------------
#endif /* new_P4 */
------------------------------------------------------------------------------

#ccall p4est_connectivity_new , CInt -> CInt -> CInt -> CInt -> IO (Ptr <struct p4est_connectivity>)
#ccall p4est_connectivity_new_copy , CInt -> CInt -> CInt -> Ptr CDouble -> Ptr CInt -> Ptr CInt -> Ptr CSChar -> Ptr CInt -> Ptr CInt -> Ptr CInt -> Ptr CSChar -> IO (Ptr <struct p4est_connectivity>)
#ccall p4est_connectivity_destroy , Ptr <struct p4est_connectivity> -> IO ()
#ccall p4est_connectivity_set_attr , Ptr <struct p4est_connectivity> -> CSize -> IO ()
#ccall p4est_connectivity_is_valid , Ptr <struct p4est_connectivity> -> IO CInt
#ccall p4est_connectivity_is_equal , Ptr <struct p4est_connectivity> -> Ptr <struct p4est_connectivity> -> IO CInt
#ccall p4est_connectivity_sink , Ptr <struct p4est_connectivity> -> Ptr <struct sc_io_sink> -> IO CInt
#ccall p4est_connectivity_deflate , Ptr <struct p4est_connectivity> -> <p4est_connectivity_encode_t> -> IO (Ptr <struct sc_array>)
#ccall p4est_connectivity_save , CString -> Ptr <struct p4est_connectivity> -> IO CInt
#ccall p4est_connectivity_source , Ptr <struct sc_io_source> -> IO (Ptr <struct p4est_connectivity>)
#ccall p4est_connectivity_inflate , Ptr <struct sc_array> -> IO (Ptr <struct p4est_connectivity>)
#ccall p4est_connectivity_load , CString -> Ptr CSize -> IO (Ptr <struct p4est_connectivity>)
#ccall p4est_connectivity_new_unitsquare , IO (Ptr <struct p4est_connectivity>)
#ccall p4est_connectivity_new_periodic , IO (Ptr <struct p4est_connectivity>)
#ccall p4est_connectivity_new_rotwrap , IO (Ptr <struct p4est_connectivity>)
#ccall p4est_connectivity_new_corner , IO (Ptr <struct p4est_connectivity>)
#ccall p4est_connectivity_new_pillow , IO (Ptr <struct p4est_connectivity>)
#ccall p4est_connectivity_new_moebius , IO (Ptr <struct p4est_connectivity>)
#ccall p4est_connectivity_new_star , IO (Ptr <struct p4est_connectivity>)
#ccall p4est_connectivity_new_cubed , IO (Ptr <struct p4est_connectivity>)
#ccall p4est_connectivity_new_disk , IO (Ptr <struct p4est_connectivity>)
#ccall p4est_connectivity_new_brick , CInt -> CInt -> CInt -> CInt -> IO (Ptr <struct p4est_connectivity>)
#ccall p4est_connectivity_new_byname , CString -> IO (Ptr <struct p4est_connectivity>)


-- * Face & corner transforms, between trees
------------------------------------------------------------------------------
#num P4EST_FTRANSFORM

#ccall p4est_expand_face_transform , CInt -> CInt -> Ptr CInt -> IO ()
#ccall p4est_find_face_transform , Ptr <struct p4est_connectivity> -> CInt -> CInt -> Ptr CInt -> IO CInt
#ccall p4est_find_corner_transform , Ptr <struct p4est_connectivity> -> CInt -> CInt -> Ptr <p4est_corner_info_t> -> IO ()


#ccall p4est_connectivity_complete , Ptr <struct p4est_connectivity> -> IO ()
#ccall p4est_connectivity_reduce , Ptr <struct p4est_connectivity> -> IO ()
#ccall p4est_connectivity_permute , Ptr <struct p4est_connectivity> -> Ptr <struct sc_array> -> CInt -> IO ()
#ccall p4est_connectivity_join_faces , Ptr <struct p4est_connectivity> -> CInt -> CInt -> CInt -> CInt -> CInt -> IO ()
#ccall p4est_connectivity_is_equivalent , Ptr <struct p4est_connectivity> -> Ptr <struct p4est_connectivity> -> IO CInt

#cinline p4est_corner_array_index , Ptr <struct sc_array> -> CSize -> IO (Ptr <p4est_corner_transform_t>)
#ccall p4est_connectivity_read_inp_stream , Ptr <struct _IO_FILE> -> Ptr CInt -> Ptr CInt -> Ptr CDouble -> Ptr CInt -> IO CInt
#ccall p4est_connectivity_read_inp , CString -> IO (Ptr <struct p4est_connectivity>)
