{-# OPTIONS_GHC -fno-warn-unused-imports #-}
#include <bindings.dsl.h>
#include "p4est_nodes.h"
module Bindings.P4est.P4estNodes where
import Foreign.Ptr
#strict_import

import Bindings.SC.ScContainers
import Bindings.SC.ScMpi
import Bindings.P4est.P4est
import Bindings.P4est.P4estGhost


{- typedef struct p4est_indep {
            p4est_qcoord_t x, y;
            int8_t level, pad8;
            int16_t pad16;
            union p4est_indep_data {
                void * unused;
                p4est_topidx_t which_tree;
                struct {
                    p4est_topidx_t which_tree; int owner_rank;
                } piggy1;
                struct {
                    p4est_topidx_t which_tree; p4est_topidx_t from_tree;
                } piggy_unused2;
                struct {
                    p4est_topidx_t which_tree; p4est_locidx_t local_num;
                } piggy3;
            } p;
        } p4est_indep_t; -}
#starttype struct p4est_indep
#field x , CInt
#field y , CInt
#field level , CSChar
#field pad8 , CSChar
#field pad16 , CShort
-- #field p.unused , Ptr ()
#field p.piggy3.which_tree , CInt
#field p.piggy3.local_num  , CInt
-- #field p , <union p4est_indep_data>
#stoptype
#synonym_t p4est_indep_t , <struct p4est_indep>

{- typedef struct p4est_hang2 {
            p4est_qcoord_t x, y;
            int8_t level, pad8;
            int16_t pad16;
            union p4est_hang2_data {
                void * unused;
                p4est_topidx_t which_tree;
                struct {
                    p4est_topidx_t which_tree; int owner_rank;
                } piggy_unused1;
                struct {
                    p4est_topidx_t which_tree; p4est_topidx_t from_tree;
                } piggy_unused2;
                struct {
                    p4est_topidx_t which_tree; p4est_locidx_t local_num;
                } piggy_unused3;
                struct {
                    p4est_topidx_t which_tree; p4est_locidx_t depends[2];
                } piggy;
            } p;
        } p4est_hang2_t; -}
#starttype struct p4est_hang2
#field x , CInt
#field y , CInt
#field level , CSChar
#field pad8 , CSChar
#field pad16 , CShort
-- #field p.unused , Ptr ()
#field p.piggy.which_tree , CInt
#field p.piggy.depends[0] , CInt
#field p.piggy.depends[1] , CInt
-- #field p , <union p4est_hang2_data>
#stoptype
#synonym_t p4est_hang2_t , <struct p4est_hang2>

{- typedef struct p4est_nodes {
            p4est_locidx_t num_local_quadrants;
            p4est_locidx_t num_owned_indeps, num_owned_shared;
            p4est_locidx_t offset_owned_indeps;
            sc_array_t indep_nodes;
            sc_array_t face_hangings;
            p4est_locidx_t * local_nodes;
            sc_array_t shared_indeps;
            p4est_locidx_t * shared_offsets;
            int * nonlocal_ranks;
            p4est_locidx_t * global_owned_indeps;
        } p4est_nodes_t; -}
#starttype struct p4est_nodes
#field num_local_quadrants , CInt
#field num_owned_indeps , CInt
#field num_owned_shared , CInt
#field offset_owned_indeps , CInt
#field indep_nodes , <struct sc_array>
#field face_hangings , <struct sc_array>
#field local_nodes , Ptr CInt
#field shared_indeps , <struct sc_array>
#field shared_offsets , Ptr CInt
#field nonlocal_ranks , Ptr CInt
#field global_owned_indeps , Ptr CInt
#stoptype
#synonym_t p4est_nodes_t , <struct p4est_nodes>

#ccall p4est_nodes_new , Ptr <struct p4est> -> Ptr <p4est_ghost_t> -> IO (Ptr <struct p4est_nodes>)
#ccall p4est_nodes_destroy , Ptr <struct p4est_nodes> -> IO ()
#ccall p4est_nodes_is_valid , Ptr <struct p4est> -> Ptr <struct p4est_nodes> -> IO CInt
