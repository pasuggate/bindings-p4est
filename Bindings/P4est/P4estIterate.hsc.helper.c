#include <bindings.cmacros.h>
#include <sc.h>
#include <p4est.h>
#include <p4est_iterate.h>

BC_INLINE2(p4est_iter_cside_array_index_int, sc_array_t*, int, p4est_iter_corner_side_t*)
BC_INLINE2(p4est_iter_cside_array_index, sc_array_t*, size_t, p4est_iter_corner_side_t*)
BC_INLINE2(p4est_iter_fside_array_index_int, sc_array_t*, int, p4est_iter_face_side_t*)
BC_INLINE2(p4est_iter_fside_array_index, sc_array_t*, size_t, p4est_iter_face_side_t*)
