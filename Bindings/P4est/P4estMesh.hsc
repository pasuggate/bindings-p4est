{-# OPTIONS_GHC -fno-warn-unused-imports #-}
#include <bindings.dsl.h>
#include "p4est_mesh.h"
module Bindings.P4est.P4estMesh where
import Foreign.Ptr
#strict_import


import Bindings.SC.ScContainers
import Bindings.P4est.P4est
import Bindings.P4est.P4estGhost
import Bindings.P4est.P4estConnectivity


{- #ccall sc_extern_c_hack_3 , IO () -}

{- typedef struct {
            p4est_locidx_t local_num_quadrants;
            p4est_locidx_t ghost_num_quadrants;
            p4est_topidx_t * quad_to_tree;
            int * ghost_to_proc;
            p4est_locidx_t * quad_to_quad;
            int8_t * quad_to_face;
            sc_array_t * quad_to_half;
            sc_array_t * quad_level;
            p4est_locidx_t local_num_corners;
            p4est_locidx_t * quad_to_corner;
            sc_array_t * corner_offset;
            sc_array_t * corner_quad;
            sc_array_t * corner_corner;
        } p4est_mesh_t; -}
#starttype p4est_mesh_t
#field local_num_quadrants , CInt
#field ghost_num_quadrants , CInt
#field quad_to_tree , Ptr CInt
#field ghost_to_proc , Ptr CInt
#field quad_to_quad , Ptr CInt
#field quad_to_face , Ptr CSChar
#field quad_to_half , Ptr <struct sc_array>
#field quad_level , Ptr <struct sc_array>
#field local_num_corners , CInt
#field quad_to_corner , Ptr CInt
#field corner_offset , Ptr <struct sc_array>
#field corner_quad , Ptr <struct sc_array>
#field corner_corner , Ptr <struct sc_array>
#stoptype
{- typedef struct {
            p4est_t * p4est;
            p4est_ghost_t * ghost;
            p4est_mesh_t * mesh;
            p4est_topidx_t which_tree;
            p4est_locidx_t quadrant_id;
            p4est_locidx_t quadrant_code;
            int face;
            int subface;
            p4est_locidx_t current_qtq;
        } p4est_mesh_face_neighbor_t; -}
#starttype p4est_mesh_face_neighbor_t
#field p4est , Ptr <struct p4est>
#field ghost , Ptr <p4est_ghost_t>
#field mesh , Ptr <p4est_mesh_t>
#field which_tree , CInt
#field quadrant_id , CInt
#field quadrant_code , CInt
#field face , CInt
#field subface , CInt
#field current_qtq , CInt
#stoptype
#ccall p4est_mesh_memory_used , Ptr <p4est_mesh_t> -> IO CSize
#ccall p4est_mesh_new , Ptr <struct p4est> -> Ptr <p4est_ghost_t> -> <p4est_connect_type_t> -> IO (Ptr <p4est_mesh_t>)
#ccall p4est_mesh_destroy , Ptr <p4est_mesh_t> -> IO ()
#ccall p4est_mesh_quadrant_cumulative , Ptr <struct p4est> -> CInt -> Ptr CInt -> Ptr CInt -> IO (Ptr <struct p4est_quadrant>)
#ccall p4est_mesh_face_neighbor_init2 , Ptr <p4est_mesh_face_neighbor_t> -> Ptr <struct p4est> -> Ptr <p4est_ghost_t> -> Ptr <p4est_mesh_t> -> CInt -> CInt -> IO ()
#ccall p4est_mesh_face_neighbor_init , Ptr <p4est_mesh_face_neighbor_t> -> Ptr <struct p4est> -> Ptr <p4est_ghost_t> -> Ptr <p4est_mesh_t> -> CInt -> Ptr <struct p4est_quadrant> -> IO ()
#ccall p4est_mesh_face_neighbor_next , Ptr <p4est_mesh_face_neighbor_t> -> Ptr CInt -> Ptr CInt -> Ptr CInt -> Ptr CInt -> IO (Ptr <struct p4est_quadrant>)
#ccall p4est_mesh_face_neighbor_data , Ptr <p4est_mesh_face_neighbor_t> -> Ptr () -> IO (Ptr ())

{- #ccall sc_extern_c_hack_4 , IO () -}
