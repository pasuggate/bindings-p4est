{-# OPTIONS_GHC -fno-warn-unused-imports #-}
#include <bindings.dsl.h>
#include "sc_keyvalue.h"
module Bindings.SC.ScKeyvalue where
import Foreign.Ptr
#strict_import


------------------------------------------------------------------------------
import Bindings.SC.Sc
import Bindings.SC.ScContainers
-- [patrick]
------------------------------------------------------------------------------


{- typedef enum {
            SC_KEYVALUE_ENTRY_NONE = 0,
            SC_KEYVALUE_ENTRY_INT,
            SC_KEYVALUE_ENTRY_DOUBLE,
            SC_KEYVALUE_ENTRY_STRING,
            SC_KEYVALUE_ENTRY_POINTER
        } sc_keyvalue_entry_type_t; -}
#integral_t sc_keyvalue_entry_type_t
#num SC_KEYVALUE_ENTRY_NONE
#num SC_KEYVALUE_ENTRY_INT
#num SC_KEYVALUE_ENTRY_DOUBLE
#num SC_KEYVALUE_ENTRY_STRING
#num SC_KEYVALUE_ENTRY_POINTER

#if 1 /* new_SC */
------------------------------------------------------------------------------
{- typedef struct sc_keyvalue sc_keyvalue_t; -}
#opaque_t struct sc_keyvalue
#synonym_t sc_keyvalue_t , <struct sc_keyvalue>
------------------------------------------------------------------------------
#ccall sc_keyvalue_new , IO (Ptr <struct sc_keyvalue>)
#ccall sc_keyvalue_newf , CInt -> IO (Ptr <struct sc_keyvalue>)
-- #ccall sc_keyvalue_newv , <__builtin_va_list> -> IO (Ptr <struct sc_keyvalue>)
#ccall sc_keyvalue_destroy , Ptr <struct sc_keyvalue> -> IO ()
#ccall sc_keyvalue_exists , Ptr <struct sc_keyvalue> -> CString -> IO <sc_keyvalue_entry_type_t>
#ccall sc_keyvalue_unset , Ptr <struct sc_keyvalue> -> CString -> IO <sc_keyvalue_entry_type_t>
#ccall sc_keyvalue_get_int , Ptr <struct sc_keyvalue> -> CString -> CInt -> IO CInt
#ccall sc_keyvalue_get_double , Ptr <struct sc_keyvalue> -> CString -> CDouble -> IO CDouble
#ccall sc_keyvalue_get_string , Ptr <struct sc_keyvalue> -> CString -> CString -> IO CString
#ccall sc_keyvalue_get_pointer , Ptr <struct sc_keyvalue> -> CString -> Ptr () -> IO (Ptr ())
#ccall sc_keyvalue_get_int_check , Ptr <struct sc_keyvalue> -> CString -> Ptr CInt -> IO CInt
#ccall sc_keyvalue_set_int , Ptr <struct sc_keyvalue> -> CString -> CInt -> IO ()
#ccall sc_keyvalue_set_double , Ptr <struct sc_keyvalue> -> CString -> CDouble -> IO ()
#ccall sc_keyvalue_set_string , Ptr <struct sc_keyvalue> -> CString -> CString -> IO ()
#ccall sc_keyvalue_set_pointer , Ptr <struct sc_keyvalue> -> CString -> Ptr () -> IO ()
#callback sc_keyvalue_foreach_t , CString -> <sc_keyvalue_entry_type_t> -> Ptr () -> Ptr () -> IO CInt
#ccall sc_keyvalue_foreach , Ptr <struct sc_keyvalue> -> <sc_keyvalue_foreach_t> -> Ptr () -> IO ()
------------------------------------------------------------------------------

#else /* !new_SC */

{- typedef struct sc_keyvalue {
            sc_hash_t * hash; sc_mempool_t * value_allocator;
        } sc_keyvalue_t; -}
#starttype struct sc_keyvalue
#field hash , Ptr <struct sc_hash>
#field value_allocator , Ptr <struct sc_mempool>
#stoptype
#synonym_t sc_keyvalue_t , <struct sc_keyvalue>
#ccall sc_keyvalue_new , IO (Ptr <struct sc_keyvalue>)
#ccall sc_keyvalue_newf , CInt -> IO (Ptr <struct sc_keyvalue>)
-- #ccall sc_keyvalue_newv , <__builtin_va_list> -> IO (Ptr <struct sc_keyvalue>)
#ccall sc_keyvalue_destroy , Ptr <struct sc_keyvalue> -> IO ()
#ccall sc_keyvalue_exists , Ptr <struct sc_keyvalue> -> CString -> IO <sc_keyvalue_entry_type_t>
#ccall sc_keyvalue_unset , Ptr <struct sc_keyvalue> -> CString -> IO <sc_keyvalue_entry_type_t>
#ccall sc_keyvalue_get_int , Ptr <struct sc_keyvalue> -> CString -> CInt -> IO CInt
#ccall sc_keyvalue_get_double , Ptr <struct sc_keyvalue> -> CString -> CDouble -> IO CDouble
#ccall sc_keyvalue_get_string , Ptr <struct sc_keyvalue> -> CString -> CString -> IO CString
#ccall sc_keyvalue_get_pointer , Ptr <struct sc_keyvalue> -> CString -> Ptr () -> IO (Ptr ())
#ccall sc_keyvalue_set_int , Ptr <struct sc_keyvalue> -> CString -> CInt -> IO ()
#ccall sc_keyvalue_set_double , Ptr <struct sc_keyvalue> -> CString -> CDouble -> IO ()
#ccall sc_keyvalue_set_string , Ptr <struct sc_keyvalue> -> CString -> CString -> IO ()
#ccall sc_keyvalue_set_pointer , Ptr <struct sc_keyvalue> -> CString -> Ptr () -> IO ()
#callback sc_keyvalue_foreach_t , CString -> <sc_keyvalue_entry_type_t> -> Ptr () -> Ptr () -> IO CInt
#ccall sc_keyvalue_foreach , Ptr <struct sc_keyvalue> -> <sc_keyvalue_foreach_t> -> Ptr () -> IO ()

#endif /* !new_SC */
