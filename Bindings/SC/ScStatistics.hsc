{-# OPTIONS_GHC -fno-warn-unused-imports #-}
#include <bindings.dsl.h>
#include "sc_statistics.h"
module Bindings.SC.ScStatistics where
import Foreign.Ptr
#strict_import


------------------------------------------------------------------------------
import Bindings.SC.Sc
import Bindings.SC.ScKeyvalue
import Bindings.SC.ScContainers
-- [patrick]
------------------------------------------------------------------------------


------------------------------------------------------------------------------
#if 1 /* new_SC */
------------------------------------------------------------------------------
#globalvar sc_stats_group_all , CInt
#globalvar sc_stats_prio_all , CInt
------------------------------------------------------------------------------
{- typedef struct sc_statinfo {
            int dirty;
            long count;
            double sum_values, sum_squares, min, max;
            int min_at_rank, max_at_rank;
            double average, variance, standev;
            double variance_mean, standev_mean;
            const char * variable;
            char * variable_owned;
            int group;
            int prio;
        } sc_statinfo_t; -}
#starttype struct sc_statinfo
#field dirty , CInt
#field count , CLong
#field sum_values , CDouble
#field sum_squares , CDouble
#field min , CDouble
#field max , CDouble
#field min_at_rank , CInt
#field max_at_rank , CInt
#field average , CDouble
#field variance , CDouble
#field standev , CDouble
#field variance_mean , CDouble
#field standev_mean , CDouble
#field variable , CString
#field variable_owned , CString
#field group , CInt
#field prio , CInt
#stoptype
#synonym_t sc_statinfo_t , <struct sc_statinfo>
------------------------------------------------------------------------------
{- typedef struct sc_stats {
            MPI_Comm mpicomm; sc_keyvalue_t * kv; sc_array_t * sarray;
        } sc_statistics_t; -}
#starttype struct sc_stats
#field mpicomm , <MPI_Comm>
#field kv , Ptr <struct sc_keyvalue>
#field sarray , Ptr <struct sc_array>
#stoptype
#synonym_t sc_statistics_t , <struct sc_stats>
------------------------------------------------------------------------------
#ccall sc_stats_set1 , Ptr <struct sc_statinfo> -> CDouble -> CString -> IO ()
#ccall sc_stats_set1_ext , Ptr <struct sc_statinfo> -> CDouble -> CString -> CInt -> CInt -> CInt -> IO ()
#ccall sc_stats_init , Ptr <struct sc_statinfo> -> CString -> IO ()
#ccall sc_stats_init_ext , Ptr <struct sc_statinfo> -> CString -> CInt -> CInt -> CInt -> IO ()
#ccall sc_stats_reset , Ptr <struct sc_statinfo> -> CInt -> IO ()
#ccall sc_stats_set_group_prio , Ptr <struct sc_statinfo> -> CInt -> CInt -> IO ()
#ccall sc_stats_accumulate , Ptr <struct sc_statinfo> -> CDouble -> IO ()
#ccall sc_stats_compute , <MPI_Comm> -> CInt -> Ptr <struct sc_statinfo> -> IO ()
#ccall sc_stats_compute1 , <MPI_Comm> -> CInt -> Ptr <struct sc_statinfo> -> IO ()
#ccall sc_stats_print , CInt -> CInt -> CInt -> Ptr <struct sc_statinfo> -> CInt -> CInt -> IO ()
#ccall sc_stats_print_ext , CInt -> CInt -> CInt -> Ptr <struct sc_statinfo> -> CInt -> CInt -> CInt -> CInt -> IO ()
#ccall sc_statistics_new , <MPI_Comm> -> IO (Ptr <struct sc_stats>)
#ccall sc_statistics_destroy , Ptr <struct sc_stats> -> IO ()
#ccall sc_statistics_add , Ptr <struct sc_stats> -> CString -> IO ()
#ccall sc_statistics_add_empty , Ptr <struct sc_stats> -> CString -> IO ()
#ccall sc_statistics_has , Ptr <struct sc_stats> -> CString -> IO CInt
#ccall sc_statistics_set , Ptr <struct sc_stats> -> CString -> CDouble -> IO ()
#ccall sc_statistics_accumulate , Ptr <struct sc_stats> -> CString -> CDouble -> IO ()
#ccall sc_statistics_compute , Ptr <struct sc_stats> -> IO ()
#ccall sc_statistics_print , Ptr <struct sc_stats> -> CInt -> CInt -> CInt -> CInt -> IO ()

------------------------------------------------------------------------------
#else /* !new_SC */
------------------------------------------------------------------------------
{- typedef struct sc_statinfo {
            int dirty;
            long count;
            double sum_values, sum_squares, min, max;
            int min_at_rank, max_at_rank;
            double average, variance, standev;
            double variance_mean, standev_mean;
            const char * variable;
        } sc_statinfo_t; -}
#starttype struct sc_statinfo
#field dirty , CInt
#field count , CLong
#field sum_values , CDouble
#field sum_squares , CDouble
#field min , CDouble
#field max , CDouble
#field min_at_rank , CInt
#field max_at_rank , CInt
#field average , CDouble
#field variance , CDouble
#field standev , CDouble
#field variance_mean , CDouble
#field standev_mean , CDouble
#field variable , CString
#stoptype
#synonym_t sc_statinfo_t , <struct sc_statinfo>

------------------------------------------------------------------------------
{- typedef struct sc_stats {
            MPI_Comm mpicomm; sc_keyvalue_t * kv; sc_array_t * sarray;
        } sc_statistics_t; -}
#starttype struct sc_stats
#field mpicomm , <MPI_Comm>
#field kv , Ptr <struct sc_keyvalue>
#field sarray , Ptr <struct sc_array>
#stoptype
#synonym_t sc_statistics_t , <struct sc_stats>
------------------------------------------------------------------------------
#ccall sc_stats_set1 , Ptr <struct sc_statinfo> -> CDouble -> CString -> IO ()
#ccall sc_stats_init , Ptr <struct sc_statinfo> -> CString -> IO ()
#ccall sc_stats_accumulate , Ptr <struct sc_statinfo> -> CDouble -> IO ()
#ccall sc_stats_compute , <MPI_Comm> -> CInt -> Ptr <struct sc_statinfo> -> IO ()
#ccall sc_stats_compute1 , <MPI_Comm> -> CInt -> Ptr <struct sc_statinfo> -> IO ()
#ccall sc_stats_print , CInt -> CInt -> CInt -> Ptr <struct sc_statinfo> -> CInt -> CInt -> IO ()
#ccall sc_statistics_new , <MPI_Comm> -> IO (Ptr <struct sc_stats>)
#ccall sc_statistics_destroy , Ptr <struct sc_stats> -> IO ()
#ccall sc_statistics_add , Ptr <struct sc_stats> -> CString -> IO ()
#ccall sc_statistics_add_empty , Ptr <struct sc_stats> -> CString -> IO ()
#ccall sc_statistics_set , Ptr <struct sc_stats> -> CString -> CDouble -> IO ()
#ccall sc_statistics_accumulate , Ptr <struct sc_stats> -> CString -> CDouble -> IO ()
#ccall sc_statistics_compute , Ptr <struct sc_stats> -> IO ()
#ccall sc_statistics_print , Ptr <struct sc_stats> -> CInt -> CInt -> CInt -> CInt -> IO ()
------------------------------------------------------------------------------

#endif /* new_SC */
