#include <bindings.cmacros.h>
#include <sc.h>
#include <sc_containers.h>

BC_INLINE2(sc_array_index, sc_array_t*, size_t, void*)
BC_INLINE2(sc_array_index_int, sc_array_t*, int, void*)
BC_INLINE2(sc_array_index_long, sc_array_t*, long, void*)
BC_INLINE2(sc_array_index_ssize_t, sc_array_t*, ssize_t, void*)
BC_INLINE2(sc_array_index_int16, sc_array_t*, int16_t, void*)
BC_INLINE2(sc_array_position, sc_array_t*, void*, size_t)
BC_INLINE1(sc_array_pop, sc_array_t*, void*)
BC_INLINE2(sc_array_push_count, sc_array_t*, size_t, void*)
BC_INLINE1(sc_array_push, sc_array_t*, void*)
BC_INLINE1(sc_mempool_alloc, sc_mempool_t*, void*)
BC_INLINE2VOID(sc_mempool_free, sc_mempool_t*, void*)
