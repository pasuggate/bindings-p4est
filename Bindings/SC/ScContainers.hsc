{-# OPTIONS_GHC -fno-warn-unused-imports #-}
#include <bindings.dsl.h>
#include "sc_containers.h"
module Bindings.SC.ScContainers where
import Foreign.Ptr
#strict_import


-- import Bindings.SC.ScObstack
-- #opaque_t obstack

-- #starttype struct obstack
-- #stoptype


#callback sc_hash_function_t , Ptr () -> Ptr () -> IO CUInt
#callback sc_equal_function_t , Ptr () -> Ptr () -> Ptr () -> IO CInt
#callback sc_hash_foreach_t , Ptr (Ptr ()) -> Ptr () -> IO CInt


-- * SC Arrays
------------------------------------------------------------------------------
-- | SC array data type.
{- typedef struct sc_array {
            size_t elem_size;
            size_t elem_count;
            ssize_t byte_alloc;
            char * array;
        } sc_array_t; -}
#starttype struct sc_array
#field elem_size , CSize
#field elem_count , CSize
#field byte_alloc , CLong
#field array , CString
#stoptype
#synonym_t sc_array_t , <struct sc_array>

#ccall sc_array_memory_used , Ptr <struct sc_array> -> CInt -> IO CSize
#ccall sc_array_new , CSize -> IO (Ptr <struct sc_array>)
-- #ccall sc_array_new_size , CSize -> CSize -> IO (Ptr <struct sc_array>)

-- TODO: Version this correctly?
-- #ccall sc_array_new_count , CSize -> CSize -> IO (Ptr <struct sc_array>)

#ccall sc_array_new_view , Ptr <struct sc_array> -> CSize -> CSize -> IO (Ptr <struct sc_array>)
#ccall sc_array_new_data , Ptr () -> CSize -> CSize -> IO (Ptr <struct sc_array>)
#ccall sc_array_destroy , Ptr <struct sc_array> -> IO ()
#ccall sc_array_init , Ptr <struct sc_array> -> CSize -> IO ()
#ccall sc_array_init_size , Ptr <struct sc_array> -> CSize -> CSize -> IO ()
#ccall sc_array_init_view , Ptr <struct sc_array> -> Ptr <struct sc_array> -> CSize -> CSize -> IO ()
#ccall sc_array_init_data , Ptr <struct sc_array> -> Ptr () -> CSize -> CSize -> IO ()
#ccall sc_array_reset , Ptr <struct sc_array> -> IO ()
#ccall sc_array_truncate , Ptr <struct sc_array> -> IO ()
#ccall sc_array_resize , Ptr <struct sc_array> -> CSize -> IO ()
#ccall sc_array_copy , Ptr <struct sc_array> -> Ptr <struct sc_array> -> IO ()
#ccall sc_array_sort , Ptr <struct sc_array> -> FunPtr (Ptr () -> Ptr () -> CInt) -> IO ()
#ccall sc_array_is_sorted , Ptr <struct sc_array> -> FunPtr (Ptr () -> Ptr () -> CInt) -> IO CInt
#ccall sc_array_is_equal , Ptr <struct sc_array> -> Ptr <struct sc_array> -> IO CInt
#ccall sc_array_uniq , Ptr <struct sc_array> -> FunPtr (Ptr () -> Ptr () -> CInt) -> IO ()
#ccall sc_array_bsearch , Ptr <struct sc_array> -> Ptr () -> FunPtr (Ptr () -> Ptr () -> CInt) -> IO CLong

#callback sc_array_type_t , Ptr <struct sc_array> -> CSize -> Ptr () -> IO CSize
#ccall sc_array_split , Ptr <struct sc_array> -> Ptr <struct sc_array> -> CSize -> <sc_array_type_t> -> Ptr () -> IO ()
#ccall sc_array_is_permutation , Ptr <struct sc_array> -> IO CInt
#ccall sc_array_permute , Ptr <struct sc_array> -> Ptr <struct sc_array> -> CInt -> IO ()
#ccall sc_array_checksum , Ptr <struct sc_array> -> IO CUInt
#ccall sc_array_pqueue_add , Ptr <struct sc_array> -> Ptr () -> FunPtr (Ptr () -> Ptr () -> CInt) -> IO CSize
#ccall sc_array_pqueue_pop , Ptr <struct sc_array> -> Ptr () -> FunPtr (Ptr () -> Ptr () -> CInt) -> IO CSize
#cinline sc_array_index , Ptr <struct sc_array> -> CSize -> IO (Ptr ())
#cinline sc_array_index_int , Ptr <struct sc_array> -> CInt -> IO (Ptr ())
#cinline sc_array_index_long , Ptr <struct sc_array> -> CLong -> IO (Ptr ())
#cinline sc_array_index_ssize_t , Ptr <struct sc_array> -> CLong -> IO (Ptr ())
#cinline sc_array_index_int16 , Ptr <struct sc_array> -> CShort -> IO (Ptr ())
#cinline sc_array_position , Ptr <struct sc_array> -> Ptr () -> IO CSize
#cinline sc_array_pop , Ptr <struct sc_array> -> IO (Ptr ())
#cinline sc_array_push_count , Ptr <struct sc_array> -> CSize -> IO (Ptr ())
#cinline sc_array_push , Ptr <struct sc_array> -> IO (Ptr ())


-- * SC memory pools
------------------------------------------------------------------------------
#if 1 /* new_SC */
------------------------------------------------------------------------------
{- typedef struct sc_mstamp {
            size_t elem_size;
            size_t per_stamp;
            size_t stamp_size;
            size_t cur_snext;
            char * current;
            sc_array_t remember;
        } sc_mstamp_t; -}
#starttype struct sc_mstamp
#field elem_size , CSize
#field per_stamp , CSize
#field stamp_size , CSize
#field cur_snext , CSize
#field current , CString
#field remember , <struct sc_array>
#stoptype
#synonym_t sc_mstamp_t , <struct sc_mstamp>
------------------------------------------------------------------------------
#ccall sc_mstamp_init , Ptr <struct sc_mstamp> -> CSize -> CSize -> IO ()
#ccall sc_mstamp_reset , Ptr <struct sc_mstamp> -> IO ()
#ccall sc_mstamp_truncate , Ptr <struct sc_mstamp> -> IO ()
#ccall sc_mstamp_alloc , Ptr <struct sc_mstamp> -> IO (Ptr ())
#ccall sc_mstamp_memory_used , Ptr <struct sc_mstamp> -> IO CSize
------------------------------------------------------------------------------
{- typedef struct sc_mempool {
            size_t elem_size;
            size_t elem_count;
            int zero_and_persist;
            sc_mstamp_t mstamp;
            sc_array_t freed;
        } sc_mempool_t; -}
#starttype struct sc_mempool
#field elem_size , CSize
#field elem_count , CSize
#field zero_and_persist , CInt
#field mstamp , <struct sc_mstamp>
#field freed , <struct sc_array>
#stoptype
#synonym_t sc_mempool_t , <struct sc_mempool>
#ccall sc_mempool_memory_used , Ptr <struct sc_mempool> -> IO CSize
#ccall sc_mempool_new , CSize -> IO (Ptr <struct sc_mempool>)
#ccall sc_mempool_new_zero_and_persist , CSize -> IO (Ptr <struct sc_mempool>)
#ccall sc_mempool_init , Ptr <struct sc_mempool> -> CSize -> IO ()
#ccall sc_mempool_destroy , Ptr <struct sc_mempool> -> IO ()
#ccall sc_mempool_destroy_null , Ptr (Ptr <struct sc_mempool>) -> IO ()
#ccall sc_mempool_reset , Ptr <struct sc_mempool> -> IO ()
#ccall sc_mempool_truncate , Ptr <struct sc_mempool> -> IO ()
#cinline sc_mempool_alloc , Ptr <struct sc_mempool> -> IO (Ptr ())
#cinline sc_mempool_free , Ptr <struct sc_mempool> -> Ptr () -> IO ()

------------------------------------------------------------------------------
#else /* !new_SC */
------------------------------------------------------------------------------

{- typedef struct sc_mempool {
            size_t elem_size;
            size_t elem_count;
            struct obstack obstack;
            sc_array_t freed;
        } sc_mempool_t; -}
#starttype struct sc_mempool
#field elem_size , CSize
#field elem_count , CSize
#field obstack , <struct obstack>
#field freed , <struct sc_array>
#stoptype
#synonym_t sc_mempool_t , <struct sc_mempool>
#ccall sc_mempool_memory_used , Ptr <struct sc_mempool> -> IO CSize
#ccall sc_mempool_new , CSize -> IO (Ptr <struct sc_mempool>)
#ccall sc_mempool_destroy , Ptr <struct sc_mempool> -> IO ()
#ccall sc_mempool_truncate , Ptr <struct sc_mempool> -> IO ()
#cinline sc_mempool_alloc , Ptr <struct sc_mempool> -> IO (Ptr ())
#cinline sc_mempool_free , Ptr <struct sc_mempool> -> Ptr () -> IO ()

#endif /* !old_SC */

{- typedef struct sc_link {
            void * data; struct sc_link * next;
        } sc_link_t; -}
#starttype struct sc_link
#field data , Ptr ()
#field next , Ptr <struct sc_link>
#stoptype
#synonym_t sc_link_t , <struct sc_link>
{- typedef struct sc_list {
            size_t elem_count;
            sc_link_t * first;
            sc_link_t * last;
            int allocator_owned;
            sc_mempool_t * allocator;
        } sc_list_t; -}
#starttype struct sc_list
#field elem_count , CSize
#field first , Ptr <struct sc_link>
#field last , Ptr <struct sc_link>
#field allocator_owned , CInt
#field allocator , Ptr <struct sc_mempool>
#stoptype
#synonym_t sc_list_t , <struct sc_list>
#ccall sc_list_memory_used , Ptr <struct sc_list> -> CInt -> IO CSize
#ccall sc_list_new , Ptr <struct sc_mempool> -> IO (Ptr <struct sc_list>)
#ccall sc_list_destroy , Ptr <struct sc_list> -> IO ()
#ccall sc_list_init , Ptr <struct sc_list> -> Ptr <struct sc_mempool> -> IO ()
#ccall sc_list_reset , Ptr <struct sc_list> -> IO ()
#ccall sc_list_unlink , Ptr <struct sc_list> -> IO ()
#ccall sc_list_prepend , Ptr <struct sc_list> -> Ptr () -> IO ()
#ccall sc_list_append , Ptr <struct sc_list> -> Ptr () -> IO ()
#ccall sc_list_insert , Ptr <struct sc_list> -> Ptr <struct sc_link> -> Ptr () -> IO ()
#ccall sc_list_remove , Ptr <struct sc_list> -> Ptr <struct sc_link> -> IO (Ptr ())
#ccall sc_list_pop , Ptr <struct sc_list> -> IO (Ptr ())
{- typedef struct sc_hash {
            size_t elem_count;
            sc_array_t * slots;
            void * user_data;
            sc_hash_function_t hash_fn;
            sc_equal_function_t equal_fn;
            size_t resize_checks, resize_actions;
            int allocator_owned;
            sc_mempool_t * allocator;
        } sc_hash_t; -}
#starttype struct sc_hash
#field elem_count , CSize
#field slots , Ptr <struct sc_array>
#field user_data , Ptr ()
#field hash_fn , <sc_hash_function_t>
#field equal_fn , <sc_equal_function_t>
#field resize_checks , CSize
#field resize_actions , CSize
#field allocator_owned , CInt
#field allocator , Ptr <struct sc_mempool>
#stoptype
#synonym_t sc_hash_t , <struct sc_hash>
#ccall sc_hash_function_string , Ptr () -> Ptr () -> IO CUInt
#ccall sc_hash_memory_used , Ptr <struct sc_hash> -> IO CSize
#ccall sc_hash_new , <sc_hash_function_t> -> <sc_equal_function_t> -> Ptr () -> Ptr <struct sc_mempool> -> IO (Ptr <struct sc_hash>)
#ccall sc_hash_destroy , Ptr <struct sc_hash> -> IO ()
#ccall sc_hash_truncate , Ptr <struct sc_hash> -> IO ()
#ccall sc_hash_unlink , Ptr <struct sc_hash> -> IO ()
#ccall sc_hash_unlink_destroy , Ptr <struct sc_hash> -> IO ()
#ccall sc_hash_lookup , Ptr <struct sc_hash> -> Ptr () -> Ptr (Ptr (Ptr ())) -> IO CInt
#ccall sc_hash_insert_unique , Ptr <struct sc_hash> -> Ptr () -> Ptr (Ptr (Ptr ())) -> IO CInt
#ccall sc_hash_remove , Ptr <struct sc_hash> -> Ptr () -> Ptr (Ptr ()) -> IO CInt
#ccall sc_hash_foreach , Ptr <struct sc_hash> -> <sc_hash_foreach_t> -> IO ()
#ccall sc_hash_print_statistics , CInt -> CInt -> Ptr <struct sc_hash> -> IO ()
{- typedef struct sc_hash_array_data {
            sc_array_t * pa;
            sc_hash_function_t hash_fn;
            sc_equal_function_t equal_fn;
            void * user_data;
            void * current_item;
        } sc_hash_array_data_t; -}
#starttype struct sc_hash_array_data
#field pa , Ptr <struct sc_array>
#field hash_fn , <sc_hash_function_t>
#field equal_fn , <sc_equal_function_t>
#field user_data , Ptr ()
#field current_item , Ptr ()
#stoptype
#synonym_t sc_hash_array_data_t , <struct sc_hash_array_data>
{- typedef struct sc_hash_array {
            sc_array_t a; sc_hash_array_data_t internal_data; sc_hash_t * h;
        } sc_hash_array_t; -}
#starttype struct sc_hash_array
#field a , <struct sc_array>
#field internal_data , <struct sc_hash_array_data>
#field h , Ptr <struct sc_hash>
#stoptype
#synonym_t sc_hash_array_t , <struct sc_hash_array>
#ccall sc_hash_array_memory_used , Ptr <struct sc_hash_array> -> IO CSize
#ccall sc_hash_array_new , CSize -> <sc_hash_function_t> -> <sc_equal_function_t> -> Ptr () -> IO (Ptr <struct sc_hash_array>)
#ccall sc_hash_array_destroy , Ptr <struct sc_hash_array> -> IO ()
#ccall sc_hash_array_is_valid , Ptr <struct sc_hash_array> -> IO CInt
#ccall sc_hash_array_truncate , Ptr <struct sc_hash_array> -> IO ()
#ccall sc_hash_array_lookup , Ptr <struct sc_hash_array> -> Ptr () -> Ptr CSize -> IO CInt
#ccall sc_hash_array_insert_unique , Ptr <struct sc_hash_array> -> Ptr () -> Ptr CSize -> IO (Ptr ())
#ccall sc_hash_array_rip , Ptr <struct sc_hash_array> -> Ptr <struct sc_array> -> IO ()

{- typedef struct sc_recycle_array {
            size_t elem_count; sc_array_t a; sc_array_t f;
        } sc_recycle_array_t; -}
#starttype struct sc_recycle_array
#field elem_count , CSize
#field a , <struct sc_array>
#field f , <struct sc_array>
#stoptype
#synonym_t sc_recycle_array_t , <struct sc_recycle_array>

#ccall sc_recycle_array_init   , Ptr <struct sc_recycle_array> -> CSize -> IO ()
#ccall sc_recycle_array_reset  , Ptr <struct sc_recycle_array> -> IO ()
#ccall sc_recycle_array_insert , Ptr <struct sc_recycle_array> -> Ptr CSize -> IO (Ptr ())
#ccall sc_recycle_array_remove , Ptr <struct sc_recycle_array> -> CSize -> IO (Ptr ())
