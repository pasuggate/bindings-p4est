{-# OPTIONS_GHC -fno-warn-unused-imports #-}
#include <bindings.dsl.h>
#include "sc_refcount.h"
module Bindings.SC.ScRefcount where

import Foreign.Ptr
#strict_import

import Bindings.SC.Sc

{- typedef struct sc_refcount {
            int package_id; int refcount;
        } sc_refcount_t; -}
#starttype struct sc_refcount
#field package_id , CInt
#field refcount , CInt
#stoptype
#synonym_t sc_refcount_t , <struct sc_refcount>

#ccall sc_refcount_init_invalid , Ptr <struct sc_refcount> -> IO ()
#ccall sc_refcount_init , Ptr <struct sc_refcount> -> CInt -> IO ()
#ccall sc_refcount_new , CInt -> IO (Ptr <struct sc_refcount>)
#ccall sc_refcount_destroy , Ptr <struct sc_refcount> -> IO ()
#ccall sc_refcount_ref , Ptr <struct sc_refcount> -> IO ()
#ccall sc_refcount_unref , Ptr <struct sc_refcount> -> IO CInt
#ccall sc_refcount_is_active , Ptr <struct sc_refcount> -> IO CInt
#ccall sc_refcount_is_last , Ptr <struct sc_refcount> -> IO CInt
