{-# OPTIONS_GHC -fno-warn-unused-imports #-}
#include <bindings.dsl.h>
#include <mpi.h>
-- #include <sc.h>
module Bindings.SC.Sc
  ( module Bindings.SC.Sc
  , module Bindings.SC.ScMpi
  ) where

#strict_import
import Foreign.Ptr
import Bindings.SC.ScMpi


#opaque_t _IO_FILE


{- typedef int sc_MPI_Comm; -}
-- #synonym_t sc_MPI_Comm , CInt
{- typedef int sc_MPI_Datatype; -}
-- #synonym_t sc_MPI_Datatype , CInt
{- typedef int sc_MPI_Op; -}
-- #synonym_t sc_MPI_Op , CInt
{- typedef int sc_MPI_Request; -}
-- #synonym_t sc_MPI_Request , CInt


#globalarray sc_log2_lookup_table , CInt

#globalvar sc_package_id , CInt
#globalvar sc_trace_file , Ptr (<struct _IO_FILE>)
#globalvar sc_trace_prio , CInt

#ccall SC_ABORTF , CString -> IO ()
#ccall SC_CHECK_ABORTF , CInt -> CString -> IO ()
#ccall SC_GEN_LOGF , CInt -> CInt -> CInt -> CString -> IO ()
#ccall SC_GLOBAL_LOGF , CInt -> CString -> IO ()
#ccall SC_LOGF , CInt -> CString -> IO ()
#ccall SC_GLOBAL_TRACEF , CString -> IO ()
#ccall SC_GLOBAL_LDEBUGF , CString -> IO ()
#ccall SC_GLOBAL_VERBOSEF , CString -> IO ()
#ccall SC_GLOBAL_INFOF , CString -> IO ()
#ccall SC_GLOBAL_STATISTICSF , CString -> IO ()
#ccall SC_GLOBAL_PRODUCTIONF , CString -> IO ()
#ccall SC_GLOBAL_ESSENTIALF , CString -> IO ()
#ccall SC_GLOBAL_LERRORF , CString -> IO ()
#ccall SC_TRACEF , CString -> IO ()
#ccall SC_LDEBUGF , CString -> IO ()
#ccall SC_VERBOSEF , CString -> IO ()
#ccall SC_INFOF , CString -> IO ()
#ccall SC_STATISTICSF , CString -> IO ()
#ccall SC_PRODUCTIONF , CString -> IO ()
#ccall SC_ESSENTIALF , CString -> IO ()
#ccall SC_LERRORF , CString -> IO ()

#callback sc_handler_t , Ptr () -> IO ()
#callback sc_log_handler_t , Ptr <struct _IO_FILE> -> CString -> CInt -> CInt -> CInt -> CInt -> CString -> IO ()
#ccall sc_malloc , CInt -> CSize -> IO (Ptr ())
#ccall sc_calloc , CInt -> CSize -> CSize -> IO (Ptr ())
#ccall sc_realloc , CInt -> Ptr () -> CSize -> IO (Ptr ())
#ccall sc_strdup , CInt -> CString -> IO CString
#ccall sc_free , CInt -> Ptr () -> IO ()
#ccall sc_memory_status , CInt -> IO CInt
#ccall sc_memory_check , CInt -> IO ()
#ccall sc_int_compare , Ptr () -> Ptr () -> IO CInt
#ccall sc_int8_compare , Ptr () -> Ptr () -> IO CInt
#ccall sc_int16_compare , Ptr () -> Ptr () -> IO CInt
#ccall sc_int32_compare , Ptr () -> Ptr () -> IO CInt
#ccall sc_int64_compare , Ptr () -> Ptr () -> IO CInt
#ccall sc_double_compare , Ptr () -> Ptr () -> IO CInt
#ccall sc_set_log_defaults , Ptr <struct _IO_FILE> -> <sc_log_handler_t> -> CInt -> IO ()
#ccall sc_log , CString -> CInt -> CInt -> CInt -> CInt -> CString -> IO ()
#ccall sc_logf , CString -> CInt -> CInt -> CInt -> CInt -> CString -> IO ()
-- #ccall sc_logv , CString -> CInt -> CInt -> CInt -> CInt -> CString -> <__builtin_va_list> -> IO ()
#ccall sc_log_indent_push_count , CInt -> CInt -> IO ()
#ccall sc_log_indent_pop_count , CInt -> CInt -> IO ()
#ccall sc_log_indent_push , IO ()
#ccall sc_log_indent_pop , IO ()
#ccall sc_abort , IO ()
#ccall sc_abort_verbose , CString -> CInt -> CString -> IO ()
#ccall sc_abort_verbosef , CString -> CInt -> CString -> IO ()
-- #ccall sc_abort_verbosev , CString -> CInt -> CString -> <__builtin_va_list> -> IO ()
#ccall sc_abort_collective , CString -> IO ()
#ccall sc_package_register , <sc_log_handler_t> -> CInt -> CString -> CString -> IO CInt
#ccall sc_package_is_registered , CInt -> IO CInt
#ccall sc_package_unregister , CInt -> IO ()
#ccall sc_package_print_summary , CInt -> IO ()

#ccall sc_init , <MPI_Comm> -> CInt -> CInt -> <sc_log_handler_t> -> CInt -> IO ()
#ccall sc_finalize , IO ()
#ccall sc_is_root , IO CInt
