{-# OPTIONS_GHC -fno-warn-unused-imports #-}
#include <bindings.dsl.h>
#include "sc_io.h"
module Bindings.SC.ScIo where
import Foreign.Ptr
#strict_import

-- [ Patrick ] ---------------------------------------------------------------
import Bindings.SC.Sc
import Bindings.SC.ScContainers
------------------------------------------------------------------------------


{- #ccall sc_extern_c_hack_3 , IO () -}

{- typedef enum {
            SC_IO_ERROR_NONE, SC_IO_ERROR_FATAL = -1, SC_IO_ERROR_AGAIN = -2
        } sc_io_error_t; -}
#integral_t sc_io_error_t
#num SC_IO_ERROR_NONE
#num SC_IO_ERROR_FATAL
#num SC_IO_ERROR_AGAIN
{- typedef enum {
            SC_IO_MODE_WRITE, SC_IO_MODE_APPEND, SC_IO_MODE_LAST
        } sc_io_mode_t; -}
#integral_t sc_io_mode_t
#num SC_IO_MODE_WRITE
#num SC_IO_MODE_APPEND
#num SC_IO_MODE_LAST
{- typedef enum {
            SC_IO_ENCODE_NONE, SC_IO_ENCODE_LAST
        } sc_io_encode_t; -}
#integral_t sc_io_encode_t
#num SC_IO_ENCODE_NONE
#num SC_IO_ENCODE_LAST
{- typedef enum {
            SC_IO_TYPE_BUFFER,
            SC_IO_TYPE_FILENAME,
            SC_IO_TYPE_FILEFILE,
            SC_IO_TYPE_LAST
        } sc_io_type_t; -}
#integral_t sc_io_type_t
#num SC_IO_TYPE_BUFFER
#num SC_IO_TYPE_FILENAME
#num SC_IO_TYPE_FILEFILE
#num SC_IO_TYPE_LAST
{- typedef struct sc_io_sink {
            sc_io_type_t iotype;
            sc_io_mode_t mode;
            sc_io_encode_t encode;
            sc_array_t * buffer;
            size_t buffer_bytes;
            FILE * file;
            size_t bytes_in;
            size_t bytes_out;
        } sc_io_sink_t; -}
#starttype struct sc_io_sink
#field iotype , <sc_io_type_t>
#field mode , <sc_io_mode_t>
#field encode , <sc_io_encode_t>
#field buffer , Ptr <struct sc_array>
#field buffer_bytes , CSize
#field file , Ptr <struct _IO_FILE>
#field bytes_in , CSize
#field bytes_out , CSize
#stoptype
#synonym_t sc_io_sink_t , <struct sc_io_sink>
{- typedef struct sc_io_source {
            sc_io_type_t iotype;
            sc_io_encode_t encode;
            sc_array_t * buffer;
            size_t buffer_bytes;
            FILE * file;
            size_t bytes_in;
            size_t bytes_out;
        } sc_io_source_t; -}
#starttype struct sc_io_source
#field iotype , <sc_io_type_t>
#field encode , <sc_io_encode_t>
#field buffer , Ptr <struct sc_array>
#field buffer_bytes , CSize
#field file , Ptr <struct _IO_FILE>
#field bytes_in , CSize
#field bytes_out , CSize
#stoptype
#synonym_t sc_io_source_t , <struct sc_io_source>
#ccall sc_io_sink_new , <sc_io_type_t> -> <sc_io_mode_t> -> <sc_io_encode_t> -> IO (Ptr <struct sc_io_sink>)
#ccall sc_io_sink_destroy , Ptr <struct sc_io_sink> -> IO CInt
#ccall sc_io_sink_write , Ptr <struct sc_io_sink> -> Ptr () -> CSize -> IO CInt
#ccall sc_io_sink_complete , Ptr <struct sc_io_sink> -> Ptr CSize -> Ptr CSize -> IO CInt
#ccall sc_io_source_new , <sc_io_type_t> -> <sc_io_encode_t> -> IO (Ptr <struct sc_io_source>)
#ccall sc_io_source_destroy , Ptr <struct sc_io_source> -> IO CInt
#ccall sc_io_source_read , Ptr <struct sc_io_source> -> Ptr () -> CSize -> Ptr CSize -> IO CInt
#ccall sc_io_source_complete , Ptr <struct sc_io_source> -> Ptr CSize -> Ptr CSize -> IO CInt
#ccall sc_vtk_write_binary , Ptr <struct _IO_FILE> -> CString -> CSize -> IO CInt
#ccall sc_vtk_write_compressed , Ptr <struct _IO_FILE> -> CString -> CSize -> IO CInt
#ccall sc_fwrite , Ptr () -> CSize -> CSize -> Ptr <struct _IO_FILE> -> CString -> IO ()
#ccall sc_fread , Ptr () -> CSize -> CSize -> Ptr <struct _IO_FILE> -> CString -> IO ()
#ccall sc_mpi_write , <MPI_File> -> Ptr () -> CSize -> <MPI_Datatype> -> CString -> IO ()

{- #ccall sc_extern_c_hack_4 , IO () -}
