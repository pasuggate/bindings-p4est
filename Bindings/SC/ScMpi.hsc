{-# LANGUAGE GeneralizedNewtypeDeriving, CPP, TemplateHaskell,
             OverloadedStrings, PatternSynonyms, ViewPatterns, RankNTypes,
             DeriveGeneric, TypeSynonymInstances, FlexibleInstances
  #-}
{-# OPTIONS_GHC -fno-warn-unused-imports #-}
#include <mpi.h>
#include <sc.h>
#include <sc_mpi.h>

#include <bindings.dsl.h>
module Bindings.SC.ScMpi where
#strict_import

-- import Foreign.Ptr

import Foreign.Storable
import Foreign.Ptr
import Foreign.C.Types
import Foreign.C.String
import GHC.Generics (Generic)
-- import Control.Distributed.MPI


------------------------------------------------------------------------------
#if 1 /* new_SC */
------------------------------------------------------------------------------
{- typedef enum {
            SC_TAG_FIRST = 's' + 'c',
            SC_TAG_AG_ALLTOALL = SC_TAG_FIRST,
            SC_TAG_AG_RECURSIVE_A,
            SC_TAG_AG_RECURSIVE_B,
            SC_TAG_AG_RECURSIVE_C,
            SC_TAG_NOTIFY_RECURSIVE,
            SC_TAG_NOTIFY_NARY = SC_TAG_NOTIFY_RECURSIVE + 32,
            SC_TAG_REDUCE = SC_TAG_NOTIFY_NARY + 32,
            SC_TAG_PSORT_LO,
            SC_TAG_PSORT_HI,
            SC_TAG_LAST
        } sc_tag_t; -}
#integral_t sc_tag_t
#num SC_TAG_FIRST
#num SC_TAG_AG_ALLTOALL
#num SC_TAG_AG_RECURSIVE_A
#num SC_TAG_AG_RECURSIVE_B
#num SC_TAG_AG_RECURSIVE_C
#num SC_TAG_NOTIFY_RECURSIVE
#num SC_TAG_NOTIFY_NARY
#num SC_TAG_REDUCE
#num SC_TAG_PSORT_LO
#num SC_TAG_PSORT_HI
#num SC_TAG_LAST

#integral_t MPI_File
#integral_t MPI_Datatype
#integral_t MPI_Op
#integral_t MPI_Request

-- #integral_t MPI_Comm
-- #num MPI_COMM_NULL
-- #synonym_t MPI_Comm , Comm
#synonym_t MPI_Comm , Ptr ()


-- #globalvar MPI_COMM_SELF  , <MPI_Comm>
-- #globalvar MPI_COMM_WORLD , <MPI_Comm>

-- #globalvar ompi_mpi_comm_self  , <MPI_Comm>
-- #globalvar ompi_mpi_comm_world , <MPI_Comm>

{- typedef int sc_MPI_Comm; -}
-- #synonym_t sc_MPI_Comm , CInt

{- typedef int sc_MPI_Group; -}
-- #synonym_t sc_MPI_Group , CInt

{- typedef int sc_MPI_Datatype; -}
-- #synonym_t sc_MPI_Datatype , CInt

{- typedef int sc_MPI_Op; -}
-- #synonym_t sc_MPI_Op , CInt

{- typedef int sc_MPI_Request; -}
-- #synonym_t sc_MPI_Request , CInt

------------------------------------------------------------------------------
#opaque_t MPI_Status
------------------------------------------------------------------------------
#ccall sc_MPI_Init , Ptr CInt -> Ptr (Ptr CString) -> IO CInt
#ccall sc_MPI_Finalize , IO CInt
#ccall sc_MPI_Abort , CInt -> CInt -> IO CInt
#ccall sc_MPI_Comm_dup , CInt -> Ptr CInt -> IO CInt
#ccall sc_MPI_Comm_create , CInt -> CInt -> Ptr CInt -> IO CInt
#ccall sc_MPI_Comm_split , CInt -> CInt -> CInt -> Ptr CInt -> IO CInt
#ccall sc_MPI_Comm_free , Ptr CInt -> IO CInt
#ccall sc_MPI_Comm_size , CInt -> Ptr CInt -> IO CInt
#ccall sc_MPI_Comm_rank , CInt -> Ptr CInt -> IO CInt

{-
#ccall sc_MPI_Comm_compare , CInt -> CInt -> Ptr CInt -> IO CInt
#ccall sc_MPI_Comm_group , CInt -> Ptr CInt -> IO CInt
#ccall sc_MPI_Group_free , Ptr CInt -> IO CInt
#ccall sc_MPI_Group_size , CInt -> Ptr CInt -> IO CInt
#ccall sc_MPI_Group_rank , CInt -> Ptr CInt -> IO CInt
#ccall sc_MPI_Group_translate_ranks , CInt -> CInt -> Ptr CInt -> CInt -> Ptr CInt -> IO CInt
#ccall sc_MPI_Group_compare , CInt -> CInt -> Ptr CInt -> IO CInt
#ccall sc_MPI_Group_union , CInt -> CInt -> Ptr CInt -> IO CInt
#ccall sc_MPI_Group_intersection , CInt -> CInt -> Ptr CInt -> IO CInt
#ccall sc_MPI_Group_difference , CInt -> CInt -> Ptr CInt -> IO CInt
#ccall sc_MPI_Group_incl , CInt -> CInt -> Ptr CInt -> Ptr CInt -> IO CInt
#ccall sc_MPI_Group_excl , CInt -> CInt -> Ptr CInt -> Ptr CInt -> IO CInt
#ccall sc_MPI_Group_range_incl , CInt -> CInt -> Ptr (Ptr CInt) -> Ptr CInt -> IO CInt
#ccall sc_MPI_Group_range_excl , CInt -> CInt -> Ptr (Ptr CInt) -> Ptr CInt -> IO CInt
#ccall sc_MPI_Barrier , CInt -> IO CInt
#ccall sc_MPI_Bcast , Ptr () -> CInt -> CInt -> CInt -> CInt -> IO CInt
#ccall sc_MPI_Gather , Ptr () -> CInt -> CInt -> Ptr () -> CInt -> CInt -> CInt -> CInt -> IO CInt
#ccall sc_MPI_Gatherv , Ptr () -> CInt -> CInt -> Ptr () -> Ptr CInt -> Ptr CInt -> CInt -> CInt -> CInt -> IO CInt
#ccall sc_MPI_Allgather , Ptr () -> CInt -> CInt -> Ptr () -> CInt -> CInt -> CInt -> IO CInt
#ccall sc_MPI_Allgatherv , Ptr () -> CInt -> CInt -> Ptr () -> Ptr CInt -> Ptr CInt -> CInt -> CInt -> IO CInt
#ccall sc_MPI_Alltoall , Ptr () -> CInt -> CInt -> Ptr () -> CInt -> CInt -> CInt -> IO CInt
#ccall sc_MPI_Reduce , Ptr () -> Ptr () -> CInt -> CInt -> CInt -> CInt -> CInt -> IO CInt
#ccall sc_MPI_Allreduce , Ptr () -> Ptr () -> CInt -> CInt -> CInt -> CInt -> IO CInt
#ccall sc_MPI_Scan , Ptr () -> Ptr () -> CInt -> CInt -> CInt -> CInt -> IO CInt
#ccall sc_MPI_Exscan , Ptr () -> Ptr () -> CInt -> CInt -> CInt -> CInt -> IO CInt
#ccall sc_MPI_Wtime , IO CDouble
#ccall sc_MPI_Recv , Ptr () -> CInt -> CInt -> CInt -> CInt -> CInt -> Ptr <struct sc_MPI_Status> -> IO CInt
#ccall sc_MPI_Irecv , Ptr () -> CInt -> CInt -> CInt -> CInt -> CInt -> Ptr CInt -> IO CInt
#ccall sc_MPI_Send , Ptr () -> CInt -> CInt -> CInt -> CInt -> CInt -> IO CInt
#ccall sc_MPI_Isend , Ptr () -> CInt -> CInt -> CInt -> CInt -> CInt -> Ptr CInt -> IO CInt
#ccall sc_MPI_Probe , CInt -> CInt -> CInt -> Ptr <struct sc_MPI_Status> -> IO CInt
#ccall sc_MPI_Iprobe , CInt -> CInt -> CInt -> Ptr CInt -> Ptr <struct sc_MPI_Status> -> IO CInt
#ccall sc_MPI_Get_count , Ptr <struct sc_MPI_Status> -> CInt -> Ptr CInt -> IO CInt
#ccall sc_MPI_Wait , Ptr CInt -> Ptr <struct sc_MPI_Status> -> IO CInt
#ccall sc_MPI_Waitsome , CInt -> Ptr CInt -> Ptr CInt -> Ptr CInt -> Ptr <struct sc_MPI_Status> -> IO CInt
#ccall sc_MPI_Waitall , CInt -> Ptr CInt -> Ptr <struct sc_MPI_Status> -> IO CInt
#ccall sc_MPI_Init_thread , Ptr CInt -> Ptr (Ptr CString) -> CInt -> Ptr CInt -> IO CInt
#ccall sc_mpi_sizeof , CInt -> IO CSize
#ccall sc_mpi_comm_attach_node_comms , CInt -> CInt -> IO ()
#ccall sc_mpi_comm_detach_node_comms , CInt -> IO ()
#ccall sc_mpi_comm_get_node_comms , CInt -> Ptr CInt -> Ptr CInt -> IO ()
-}

------------------------------------------------------------------------------
#else /* !new_SC */
------------------------------------------------------------------------------

{- typedef enum {
            SC_TAG_AG_ALLTOALL = 's' + 'c',
            SC_TAG_AG_RECURSIVE_A,
            SC_TAG_AG_RECURSIVE_B,
            SC_TAG_AG_RECURSIVE_C,
            SC_TAG_NOTIFY_RECURSIVE,
            SC_TAG_REDUCE,
            SC_TAG_PSORT_LO,
            SC_TAG_PSORT_HI
        } sc_tag_t; -}
#integral_t sc_tag_t
#num SC_TAG_AG_ALLTOALL
#num SC_TAG_AG_RECURSIVE_A
#num SC_TAG_AG_RECURSIVE_B
#num SC_TAG_AG_RECURSIVE_C
#num SC_TAG_NOTIFY_RECURSIVE
#num SC_TAG_REDUCE
#num SC_TAG_PSORT_LO
#num SC_TAG_PSORT_HI

#integral_t MPI_File
#integral_t MPI_Datatype
#integral_t MPI_Op
#integral_t MPI_Request

#integral_t MPI_Comm
#num MPI_COMM_NULL

-- #globalvar MPI_COMM_SELF  , <MPI_Comm>
-- #globalvar MPI_COMM_WORLD , <MPI_Comm>

#globalvar ompi_mpi_comm_self  , <MPI_Comm>
#globalvar ompi_mpi_comm_world , <MPI_Comm>

-- #globalarray MPI_COMM_SELF  , <MPI_Comm>
-- #globalarray MPI_COMM_WORLD , <MPI_Comm>

{-
#opaque_t MPI_Comm
#opaque_t MPI_Datatype
#opaque_t MPI_Op
#opaque_t MPI_Request
-}

{- typedef int sc_MPI_Comm; -}
-- #synonym_t sc_MPI_Comm , Ptr <MPI_Comm>
-- #synonym_t sc_MPI_Comm , CInt
{- typedef int sc_MPI_Datatype; -}
-- #synonym_t sc_MPI_Datatype , Ptr <MPI_Datatype>
-- #synonym_t sc_MPI_Datatype , CInt
{- typedef int sc_MPI_Op; -}
-- #synonym_t sc_MPI_Op , CInt
-- #synonym_t sc_MPI_Op , Ptr <MPI_Op>
{- typedef int sc_MPI_Request; -}
-- #synonym_t sc_MPI_Request , CInt
-- #synonym_t sc_MPI_Request , Ptr <MPI_Request>


-- * Data types for asynchronous operations
------------------------------------------------------------------------------
{- typedef int sc_MPI_Request; -}
#synonym_t sc_MPI_Request , CInt

{- typedef struct sc_MPI_Status {
            int count;
            int cancelled;
            int MPI_SOURCE;
            int MPI_TAG;
            int MPI_ERROR;
        } sc_MPI_Status; -}
{-- }
#starttype struct MPI_Status
#field count , CInt
#field cancelled , CInt
#field MPI_SOURCE , CInt
#field MPI_TAG , CInt
#field MPI_ERROR , CInt
#stoptype
#synonym_t MPI_Status , <struct MPI_Status>
--}
#opaque_t MPI_Status
------------------------------------------------------------------------------


#ccall sc_MPI_Init , Ptr CInt -> Ptr (Ptr CString) -> IO CInt
#ccall sc_MPI_Finalize , IO CInt
#ccall sc_MPI_Abort , <MPI_Comm> -> CInt -> IO CInt
#ccall sc_MPI_Comm_dup , <MPI_Comm> -> Ptr CInt -> IO CInt
#ccall sc_MPI_Comm_free , Ptr <MPI_Comm> -> IO CInt
#ccall sc_MPI_Comm_size , <MPI_Comm> -> Ptr CInt -> IO CInt
#ccall sc_MPI_Comm_rank , <MPI_Comm> -> Ptr CInt -> IO CInt

{-
#ccall sc_MPI_Recv , Ptr () -> CInt -> <MPI_Datatype> -> CInt -> CInt -> <MPI_Comm> -> Ptr <struct sc_MPI_Status> -> IO CInt
#ccall sc_MPI_Send , Ptr () -> CInt -> <MPI_Datatype> -> CInt -> CInt -> <MPI_Comm> -> IO CInt
#ccall sc_MPI_Bcast , Ptr () -> CInt -> <MPI_Datatype> -> CInt -> <MPI_Comm> -> IO CInt
#ccall sc_MPI_Barrier , <MPI_Comm> -> IO CInt

#ccall sc_MPI_Wtick , IO CDouble
#ccall sc_MPI_Wtime , IO CDouble
#ccall sc_MPI_Wait , Ptr <MPI_Request> -> Ptr <struct sc_MPI_Status> -> IO CInt
#ccall sc_MPI_Waitsome , CInt -> Ptr <MPI_Request> -> Ptr CInt -> Ptr CInt -> Ptr <struct sc_MPI_Status> -> IO CInt
#ccall sc_MPI_Waitall , CInt -> Ptr <MPI_Request> -> Ptr <struct sc_MPI_Status> -> IO CInt

#ccall sc_MPI_Gather , Ptr () -> CInt -> CInt -> Ptr () -> CInt -> CInt -> CInt -> <MPI_Comm> -> IO CInt
#ccall sc_MPI_Gatherv , Ptr () -> CInt -> <MPI_Datatype> -> Ptr () -> Ptr CInt -> Ptr CInt -> CInt -> <MPI_Datatype> -> <MPI_Comm> -> IO CInt
#ccall sc_MPI_Allgather , Ptr () -> <MPI_Datatype> -> CInt -> Ptr () -> CInt -> <MPI_Datatype> -> <MPI_Comm> -> IO CInt
#ccall sc_MPI_Allgatherv , Ptr () -> CInt -> CInt -> Ptr () -> Ptr CInt -> Ptr CInt -> CInt -> CInt -> IO CInt
#ccall sc_MPI_Reduce , Ptr () -> Ptr () -> CInt -> CInt -> CInt -> CInt -> CInt -> IO CInt
#ccall sc_MPI_Allreduce , Ptr () -> Ptr () -> CInt -> <MPI_Datatype> -> <MPI_Op> -> <MPI_Comm> -> IO CInt
-}

{-
#ccall sc_MPI_Irecv , Ptr () -> CInt -> CInt -> CInt -> CInt -> CInt -> Ptr CInt -> IO CInt
#ccall sc_MPI_Isend , Ptr () -> CInt -> CInt -> CInt -> CInt -> CInt -> Ptr CInt -> IO CInt
#ccall sc_MPI_Probe , <MPI_Comm> -> CInt -> CInt -> Ptr <struct sc_MPI_Status> -> IO CInt
#ccall sc_MPI_Iprobe , <MPI_Comm> -> CInt -> CInt -> Ptr CInt -> Ptr <struct sc_MPI_Status> -> IO CInt
#ccall sc_MPI_Get_count , Ptr <struct sc_MPI_Status> -> CInt -> Ptr CInt -> IO CInt
#ccall sc_MPI_Init_thread , Ptr CInt -> Ptr (Ptr CString) -> CInt -> Ptr CInt -> IO CInt
#ccall sc_mpi_sizeof , <MPI_Comm> -> IO CSize
-}

------------------------------------------------------------------------------
#endif /* new_SC */
------------------------------------------------------------------------------
