{-# LANGUAGE CPP, ForeignFunctionInterface, TupleSections, DeriveGeneric,
             GeneralizedNewtypeDeriving, OverloadedStrings, PatternSynonyms,
             ViewPatterns
  #-}

#include <mpi.h>
#include <sc.h>
#include <sc_mpi.h>
#include <bindings.dsl.h>
module Bindings.SC.ScCore where

#strict_import
import Foreign.Storable
import Foreign.Ptr
import Foreign.Marshal
import Foreign.C.Types
import GHC.Generics (Generic)


-- * Type aliases.
------------------------------------------------------------------------------
{-
newtype MpiResult =
  MpiResult { getMpiResult :: CInt }
  deriving (Eq, Ord, Enum, Bounded, Read, Show, Generic, Storable)

#ccall   ChkMpi , MpiResult -> IO MpiResult

chkMpi :: MpiResult -> IO ()
{-# INLINE chkMpi #-}
chkMpi mpiret = c'ChkMpi mpiret >> return ()
-}

#ccall   ChkMpi , CInt -> IO CInt


-- * Function aliases.
------------------------------------------------------------------------------
chkMpi :: CInt -> IO ()
{-# INLINE chkMpi #-}
chkMpi mpiret = c'ChkMpi mpiret >> return ()
