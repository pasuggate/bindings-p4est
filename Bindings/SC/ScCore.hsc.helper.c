#include <bindings.cmacros.h>
#include <mpi.h>
#include <sc.h>
#include <sc_mpi.h>


BC_GLOBALARRAY(sc_log2_lookup_table,int)
// BC_GLOBALARRAY(MPI_COMM_SELF,MPI_Comm)
// BC_GLOBALARRAY(MPI_COMM_WORLD,MPI_Comm)


int ChkMpi(int mpiret)
{
  SC_CHECK_MPI(mpiret);
  return mpiret;
}
