% README for `bindings-p4est`
---
author: Patrick Suggate <<patrick.suggate@gmail.com>>
date: 18 March, 2019
colorlinks: true
#toc: true
#toc-depth: 3
#toccolor: purple
---

<!--
\clearpage
-->

# Bindings to `p4est` #

The beginnings of raw bindings to `p4est`, generated initially using `c2hsc`.

## Compile and Install `p4est` ##

First, install a suitable MPI library:

```bash
$ sudo aptitude install libopenmpi-dev
```

Now, clone the `p4est` repository, run the bootstrap script, and then configure with MPI enabled:

```bash
$ git clone --recursive https://github.com/cburstedde/p4est.git
$ cd p4est
$ ./bootstrap
$ ./configure --prefix=/usr/local/ --enable-mpi
$ make -j10
$ sudo make install
```

and now you should have a working copy of `p4est` in `/usr/local/`.

## Cabal Build ##

The *SC* headers need to be able to find the *MPI* headers, `mpi.h`, etc. For example:

```bash
$ C_INCLUDE_PATH=/usr/include/openmpi cabal new-build -j
```

or perhaps on later Ubuntu distributions:

```bash
C_INCLUDE_PATH=/usr/lib/x86_64-linux-gnu/openmpi/include/ cabal new-build -j
```

## Old Cabal Build ##

First, you may need to install a couple of packages:

```bash
$ sudo aptitude install libp4est-dev libopenmpi-dev
```

The *SC* headers need to be able to find the *MPI* headers, `mpi.h`, etc. For example:

```bash
$ C_INCLUDE_PATH=/usr/include/openmpi cabal new-build -j
```

or perhaps on later Ubuntu distributions:

```bash
C_INCLUDE_PATH=/usr/lib/x86_64-linux-gnu/openmpi/include/ cabal new-build -j
```

## Raw Bindings to the `p4est` Library ##

Initially, `c2hsc` was used to create the `*.hsc` files, typically using:

```bash
$ c2hsc --prefix=Bindings.SC --cppopts=-I$(MPI_PATH) $(SC_PATH)/sc.h
```

But this will overwrite any customisations made to the `*.hsc` files! If you really want to do this, then run:

```bash
$ make bindings
```

from the top-level directory.

After generating the raw bindings, the dependencies for each of the `*.hsc` files need to be added; e.g., to `Bindings/P4est/P4est.hsc`, add the following:

```haskell
import Bindings.SC.Sc
import Bindings.SC.ScContainers

import Bindings.P4est.P4estConnectivity
```

and comment out the `extern C`{.c} hacks:

```haskell
{- #ccall sc_extern_c_hack_3 , IO () -}

...

{- #ccall sc_extern_c_hack_4 , IO () -}
```

Satisfying each modules' dependencies, and commenting out these hacks, needs to be performed for each auto-generated `<MODULE>.hsc` file. The include-file dependencies will need to be updated for each of the `*.helper.c` files as well (including for the *SC* library).

## TODO ##

+ use `c2hs` instead of `c2hsc`?
+ better MPI handling?
