MD	:= $(wildcard *.md)
PDF	:= $(MD:.md=.pdf)
TMP	?= doc/_layout.latex

# TODO: Setup a build folder first?

MPI_PATH := /usr/include/openmpi
P4E_PATH := /usr/include


.PHONY:	all	bindings

all:	$(PDF)
	cabal new-build -j

bindings:
	$(MAKE) -C Bindings

# build:
# 	cd build
# 	make 

.PHONY: clean
clean:
	$(MAKE) -C Bindings clean


# Implicit rules:
%.pdf: %.md $(TMP)
	+pandoc --template $(TMP) -f markdown+tex_math_double_backslash -t latex -V papersize:a4 -V geometry:margin=2cm $< -o $@
